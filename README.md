# Google Maps Connector

Google Maps Connector 1.0.0 from Ksquare gives out-of box solution to integrate Google Maps data with other business applications.

Easily design and build and integrate with mule flows to control and access google maps data.

Google Maps Connector implements the following features for the MuleSoft-based enterprise solutions:

- Directions API
- Distance Matrix API
- Elevation API
- Geocoding API
- Maps Static API
- Places API
- Roads API
- Time Zone API


Application/Service  |	Version
-------------------  | ----------
Mule Runtime	     |  4.X
Google Maps Services |  Google Maps Services v.0.15.0 Java Client
Java	             |  1.8 and later


## Highlights of Google Maps Connector Mule 4

It allows users to automatically build queries to different Google Maps API's using Mule. We can select the parameters for each operation of the Google Maps API according to Google Maps Documentation to build a query to the API's.

Add this dependency to your application pom.xml

```
<groupId>org.mule.modules.googlemaps</groupId>
<artifactId>mule-googlemaps-connector</artifactId>
<version>1.0.0</version>
<classifier>mule-plugin</classifier>
```
