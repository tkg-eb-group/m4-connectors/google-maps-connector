Google Maps Connector Demo
====================================
Anypoint Studio demo for Google Maps connector.


Prerequisites
---------------

* Anypoint Studio 7 with Mule ESB 4.x Runtime.
* Mule Google Maps Connector v1.0.0


How to Run Sample
-----------------

1. Import the project folder demo in Studio.
2. Add the application.properties to the resources folder for the API key.
3. Click on 'Test Connection' to make sure the connection works correctly.
4. Run the application.


About the Sample
----------------

Using a browser, navigate to http://localhost:8081/google-maps/demo
