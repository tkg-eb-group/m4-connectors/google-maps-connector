/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.api.param;

import org.mule.extension.google.maps.api.param.interfaces.LocationBiasParameter;
import org.mule.extension.google.maps.internal.util.GoogleConstantsUtil;
import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Alias;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.dsl.xml.ParameterDsl;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

@Alias("Rectangular")
public class RectangularParameter implements LocationBiasParameter {

    @Parameter
    @Summary("Northeast Location")
    @DisplayName("Northeast Location")
    @Expression(ExpressionSupport.NOT_SUPPORTED)
    @ParameterDsl(allowReferences = false)
    private LatitudeLongitudeParameter northEastLatLngParameter;

    @Parameter
    @Summary("Southewest Location")
    @DisplayName("Southewest Location")
    @Expression(ExpressionSupport.NOT_SUPPORTED)
    @ParameterDsl(allowReferences = false)
    private LatitudeLongitudeParameter southwestLatitudeLongitudeParameter;

    public LatitudeLongitudeParameter getNorthEastLatLngParameter() {
        return northEastLatLngParameter;
    }

    public LatitudeLongitudeParameter getSouthwestLatitudeLongitudeParameter() {
        return southwestLatitudeLongitudeParameter;
    }

    @Override
    public String type() {
        return GoogleConstantsUtil.RECTANGULAR;
    }
}
