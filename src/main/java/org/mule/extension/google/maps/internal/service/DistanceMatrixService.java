/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.internal.service;

import com.google.maps.DirectionsApi;
import com.google.maps.DistanceMatrixApi;
import com.google.maps.DistanceMatrixApiRequest;
import com.google.maps.GeoApiContext;
import com.google.maps.errors.ApiException;
import com.google.maps.model.*;
import org.mule.extension.google.maps.api.domain.*;
import org.mule.extension.google.maps.api.param.*;
import org.mule.extension.google.maps.api.param.interfaces.DateTimeParameter;
import org.mule.extension.google.maps.api.param.interfaces.DistanceMatrixOriginDestinationParameter;
import org.mule.extension.google.maps.internal.util.ExceptionUtil;
import org.mule.extension.google.maps.internal.util.GoogleConstantsUtil;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class DistanceMatrixService {

    public DistanceMatrix getDistanceMatrix(GeoApiContext context, List<DistanceMatrixOriginDestinationParameter> originsParameter,
                                List<DistanceMatrixOriginDestinationParameter> destinationsParameter, TravelType travelMode,
                                    AvoidRestriction avoid, String language, UnitSystem unit,
                                    DateTimeParameter dateTime, TrafficType trafficModel,
                                    Set<TransitModeParameter> transitModes, TransitRoutingSetting transitRoutingPreference){
        DistanceMatrixApiRequest distanceMatrixApiRequest = DistanceMatrixApi.newRequest(context);
        setOriginsParameter(originsParameter, distanceMatrixApiRequest);
        setDestinationsParameter(destinationsParameter, distanceMatrixApiRequest);

        setDistanceMatrixParameters(travelMode, avoid, language, unit, dateTime, trafficModel, transitModes, transitRoutingPreference, distanceMatrixApiRequest);
        try {
            return distanceMatrixApiRequest.await();
        } catch (ApiException | IOException ex) {
            throw new ExceptionUtil().throwGoogleMapsException(ex);
        } catch (InterruptedException ex){
            Thread.currentThread().interrupt();
            throw new ExceptionUtil().throwGoogleMapsException(ex);
        }
    }

    private void setDistanceMatrixParameters(TravelType travelMode, AvoidRestriction avoid, String language, UnitSystem unit, DateTimeParameter dateTime, TrafficType trafficModel, Set<TransitModeParameter> transitModes, TransitRoutingSetting transitRoutingPreference, DistanceMatrixApiRequest distanceMatrixApiRequest) {
        if(travelMode != null && !travelMode.getMode().isEmpty()){
            distanceMatrixApiRequest.mode(Enum.valueOf(TravelMode.class, travelMode.name()));
        }
        if(avoid != null && !avoid.getValue().isEmpty()){
            distanceMatrixApiRequest.avoid(Enum.valueOf(DirectionsApi.RouteRestriction.class, avoid.name()));
        }
        setCommonParameters(language, unit, distanceMatrixApiRequest);
        setDateTimeParameter(dateTime, distanceMatrixApiRequest);
        if(trafficModel != null && !trafficModel.getName().isEmpty()){
            distanceMatrixApiRequest.trafficModel(Enum.valueOf(TrafficModel.class, trafficModel.name()));
        }
        if(!transitModes.isEmpty()){
            TransitMode[] transitModesArray = transitModes.stream().map(transitModeParameter -> Enum.valueOf(TransitMode.class, transitModeParameter.getTransitMode().name())).toArray(TransitMode[]::new);
            distanceMatrixApiRequest.transitModes(transitModesArray);
        }
        if(transitRoutingPreference != null && !transitRoutingPreference.getValue().isEmpty()){
            distanceMatrixApiRequest.transitRoutingPreference(Enum.valueOf(TransitRoutingPreference.class, transitRoutingPreference.name()));
        }
    }

    private void setCommonParameters(String language, UnitSystem unit, DistanceMatrixApiRequest distanceMatrixApiRequest) {
        if(language != null && !language.isEmpty()){
            distanceMatrixApiRequest.language(language);
        }
        if(unit != null && !unit.getSystem().isEmpty()){
            distanceMatrixApiRequest.units(Enum.valueOf(Unit.class, unit.name()));
        }
    }

    private void setDateTimeParameter(DateTimeParameter dateTime, DistanceMatrixApiRequest distanceMatrixApiRequest) {
        if(dateTime != null){
            if(dateTime.getType().equals(GoogleConstantsUtil.ARRIVAL_DATE_TIME_TYPE)){
                ArrivalDateTimeParameter arrivalTime = (ArrivalDateTimeParameter) dateTime;
                ZonedDateTime zonedDateTime = ZonedDateTime.of(arrivalTime.getArrivalDateTime(), ZoneId.of("UTC"));
                distanceMatrixApiRequest.arrivalTime(zonedDateTime.toInstant());
            }
            else {
                DepartureDateTimeParameter departureTime = (DepartureDateTimeParameter) dateTime;
                LocalDateTime departureDateTime2 = departureTime.getDepartureDateTime();
                distanceMatrixApiRequest.departureTime(departureDateTime2.toInstant(ZoneOffset.UTC));
            }
        }
    }

    private void setOriginsParameter(List<DistanceMatrixOriginDestinationParameter> originsParameter, DistanceMatrixApiRequest distanceMatrixApiRequest) {
        List<String> origins = new ArrayList<>();
        for(DistanceMatrixOriginDestinationParameter originParameter : originsParameter){
            switch (originParameter.getType()){
                case GoogleConstantsUtil.ORIGIN_DESTINATION_ADDRESS_TYPE:
                    DistanceAddressParameter originAddressParameter = (DistanceAddressParameter) originParameter;
                    origins.add(originAddressParameter.getAddress());
                    break;
                case GoogleConstantsUtil.ORIGIN_DESTINATION_LATLNG:
                    DistanceLatitudeLongitudeParameter originLatLngParameter = (DistanceLatitudeLongitudeParameter) originParameter;
                    origins.add(originLatLngParameter.getLatitude() + "," + originLatLngParameter.getLongitude());
                    break;
                case GoogleConstantsUtil.ORIGIN_DESTINATION_PLACEID_TYPE:
                    DistancePlaceIdParameter originOriginDestinationPlaceIdParameter = (DistancePlaceIdParameter) originParameter;
                    origins.add("place_id:" + originOriginDestinationPlaceIdParameter.getPlaceId());
                    break;
                case GoogleConstantsUtil.REQUEST_POLYLINE_TYPE:
                    DistancePolylineParameter polylineCoordinateParameter = (DistancePolylineParameter) originParameter;
                    origins.add("enc:" + polylineCoordinateParameter.getPolylineCoordinates());
                    break;
                default:
                    break;
            }
        }
        if(!origins.isEmpty()){
            distanceMatrixApiRequest.origins(origins.toArray(new String[0]));
        }
    }

    private void setDestinationsParameter(List<DistanceMatrixOriginDestinationParameter> destinationsParameter, DistanceMatrixApiRequest distanceMatrixApiRequest) {
        List<String> destinations = new ArrayList<>();
        for(DistanceMatrixOriginDestinationParameter destinationParameter : destinationsParameter){
            switch (destinationParameter.getType()){
                case GoogleConstantsUtil.ORIGIN_DESTINATION_ADDRESS_TYPE:
                    DistanceAddressParameter destinationAddressParameter = (DistanceAddressParameter) destinationParameter;
                    destinations.add(destinationAddressParameter.getAddress());
                    break;
                case GoogleConstantsUtil.ORIGIN_DESTINATION_LATLNG:
                    DistanceLatitudeLongitudeParameter originLatLngParameter = (DistanceLatitudeLongitudeParameter) destinationParameter;
                    destinations.add(originLatLngParameter.getLatitude() + "," + originLatLngParameter.getLongitude());
                    break;
                case GoogleConstantsUtil.ORIGIN_DESTINATION_PLACEID_TYPE:
                    DistancePlaceIdParameter destinationPlaceIdParameter = (DistancePlaceIdParameter) destinationParameter;
                    destinations.add("place_id:" + destinationPlaceIdParameter.getPlaceId());
                    break;
                case GoogleConstantsUtil.REQUEST_POLYLINE_TYPE:
                    DistancePolylineParameter polylineCoordinateParameter = (DistancePolylineParameter) destinationParameter;
                    destinations.add("enc:" + polylineCoordinateParameter.getPolylineCoordinates());
                    break;
                default:
                    break;
            }
        }
        if(!destinations.isEmpty()){
            distanceMatrixApiRequest.destinations(destinations.toArray(new String[0]));
        }
    }
}
