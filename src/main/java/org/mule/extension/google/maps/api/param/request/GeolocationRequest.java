/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.api.param.request;

import org.mule.extension.google.maps.api.domain.RadioType;
import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.dsl.xml.ParameterDsl;
import org.mule.runtime.extension.api.annotation.param.NullSafe;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

import java.io.Serializable;
import java.util.List;

public class GeolocationRequest implements Serializable {

    @Parameter
    @Summary("The mobile country code (MCC) for the device's home network")
    @DisplayName("Home Mobile Country Code")
    @Expression(ExpressionSupport.SUPPORTED)
    @ParameterDsl(allowReferences = false)
    @Optional
    private Integer homeMobileCountryCode;

    @Parameter
    @Summary("The mobile network code (MNC) for the device's home network")
    @DisplayName("Home Mobile Network Code")
    @Expression(ExpressionSupport.SUPPORTED)
    @ParameterDsl(allowReferences = false)
    @Optional
    private Integer homeMobileNetworkCode;

    @Parameter
    @Summary("The mobile radio type")
    @DisplayName("Radio Type")
    @Expression(ExpressionSupport.SUPPORTED)
    @ParameterDsl(allowReferences = false)
    @Optional
    private RadioType radioType;

    @Parameter
    @Summary("The carrier name")
    @DisplayName("Carrier")
    @Expression(ExpressionSupport.SUPPORTED)
    @ParameterDsl(allowReferences = false)
    @Optional
    private String carrier;

    @Parameter
    @Summary("Specifies whether to fall back to IP geolocation if wifi and cell tower signals are not available")
    @DisplayName("Consider IP")
    @Expression(ExpressionSupport.SUPPORTED)
    @ParameterDsl(allowReferences = false)
    @Optional(defaultValue = "true")
    private boolean considerIp;

    @Parameter
    @Summary("An array of cell tower objects")
    @DisplayName("Cell Towers")
    @NullSafe
    @Expression(ExpressionSupport.NOT_SUPPORTED)
    @ParameterDsl(allowReferences = false)
    @Optional
    private List<CellTowerObjectsRequest> cellTowers;

    @Parameter
    @Summary("An array of WiFi access point objects")
    @DisplayName("WiFi Access Points")
    @NullSafe
    @Expression(ExpressionSupport.NOT_SUPPORTED)
    @ParameterDsl(allowReferences = false)
    @Optional
    private List<WiFiAccessPointObjectsRequest> wifiAccessPoints;

    public Integer getHomeMobileCountryCode() {
        return homeMobileCountryCode;
    }

    public Integer getHomeMobileNetworkCode() {
        return homeMobileNetworkCode;
    }

    public RadioType getRadioType() {
        return radioType;
    }

    public String getCarrier() {
        return carrier;
    }

    public boolean isConsiderIp() {
        return considerIp;
    }

    public List<CellTowerObjectsRequest> getCellTowers() {
        return cellTowers;
    }

    public List<WiFiAccessPointObjectsRequest> getWifiAccessPoints() {
        return wifiAccessPoints;
    }
}
