/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.internal.util;

import com.google.maps.errors.*;
import org.mule.extension.google.maps.internal.error.exception.GoogleMapsException;
import org.mule.extension.google.maps.internal.error.GoogleMapsErrorType;
import org.mule.runtime.http.api.HttpConstants;

public class ExceptionUtil {

    public GoogleMapsException throwGoogleMapsException(Exception exception){
        if(exception instanceof ApiException){
            if (exception instanceof InvalidRequestException) {
                String exceptionMessage = exception.getMessage() != null ? exception.getMessage() : "Bad Request";
                return new GoogleMapsException(exceptionMessage, GoogleMapsErrorType.INVALID_REQUEST, HttpConstants.HttpStatus.BAD_REQUEST);
            } else if (exception instanceof MaxElementsExceededException) {
                String exceptionMessage = exception.getMessage() != null ? exception.getMessage() : "Max Elements Exceeded";
                return new GoogleMapsException(exceptionMessage, GoogleMapsErrorType.MAX_ELEMENTS_EXCEEDED, HttpConstants.HttpStatus.BAD_REQUEST);
            } else if (exception instanceof MaxRouteLengthExceededException) {
                String exceptionMessage = exception.getMessage() != null ? exception.getMessage() : "Max Route Length Exceeded";
                return new GoogleMapsException(exceptionMessage, GoogleMapsErrorType.MAX_ROUTE_LENGTH_EXCEEDED, HttpConstants.HttpStatus.BAD_REQUEST);
            } else return throwOtherUncommonException(exception);
        }
        return new GoogleMapsException(exception.getMessage(), GoogleMapsErrorType.INTERNAL_SERVER_ERROR, HttpConstants.HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private GoogleMapsException throwOtherUncommonException(Exception exception) {
        if (exception instanceof MaxWaypointsExceededException) {
            String exceptionMessage = exception.getMessage() != null ? exception.getMessage() : "Max Waypoints Exceeded";
            return new GoogleMapsException(exceptionMessage, GoogleMapsErrorType.MAX_WAYPOINTS_EXCEEDED, HttpConstants.HttpStatus.BAD_REQUEST);
        } else if (exception instanceof NotFoundException) {
            String exceptionMessage = exception.getMessage() != null ? exception.getMessage() : "Not Found";
            return new GoogleMapsException(exceptionMessage, GoogleMapsErrorType.NOT_FOUND, HttpConstants.HttpStatus.NOT_FOUND);
        } else if (exception instanceof OverDailyLimitException) {
            String exceptionMessage = exception.getMessage() != null ? exception.getMessage() : "Over Daily Limit Exceeded";
            return new GoogleMapsException(exceptionMessage, GoogleMapsErrorType.OVER_DAILY_LIMIT, HttpConstants.HttpStatus.FORBIDDEN);
        } else if (exception instanceof OverQueryLimitException) {
            String exceptionMessage = exception.getMessage() != null ? exception.getMessage() : "Over Query Limit Exceeded";
            return new GoogleMapsException(exceptionMessage, GoogleMapsErrorType.OVER_QUERY_LIMIT, HttpConstants.HttpStatus.FORBIDDEN);
        } else return throwLessCommonException(exception);
    }

    private GoogleMapsException throwLessCommonException(Exception exception) {
        if (exception instanceof RequestDeniedException) {
            String exceptionMessage = exception.getMessage() != null ? exception.getMessage() : "Request Denied";
            return new GoogleMapsException(exceptionMessage, GoogleMapsErrorType.REQUEST_DENIED, HttpConstants.HttpStatus.UNAUTHORIZED);
        } else if (exception instanceof ZeroResultsException) {
            String exceptionMessage = exception.getMessage() != null ? exception.getMessage() : "No Results";
            return new GoogleMapsException(exceptionMessage, GoogleMapsErrorType.ZERO_RESULTS, HttpConstants.HttpStatus.NOT_FOUND);
        } else if (exception instanceof AccessNotConfiguredException) {
            String exceptionMessage = exception.getMessage() != null ? exception.getMessage() : "Access Not Configured";
            return new GoogleMapsException(exceptionMessage, GoogleMapsErrorType.ACCESS_NOT_CONFIGURED, HttpConstants.HttpStatus.CONFLICT);
        } else if (exception instanceof UnknownErrorException) {
            String exceptionMessage = exception.getMessage() != null ? exception.getMessage() : "Unknown Error";
            return new GoogleMapsException(exceptionMessage, GoogleMapsErrorType.UNKNOWN_ERROR, HttpConstants.HttpStatus.INTERNAL_SERVER_ERROR);
        } else return new GoogleMapsException(exception.getMessage(), GoogleMapsErrorType.INTERNAL_SERVER_ERROR, HttpConstants.HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
