/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.api.param;

import org.mule.extension.google.maps.api.domain.SearchPlaceField;
import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Alias;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

@Alias("Place Field")
public class SearchPlaceFieldsParameter {

    @Parameter
    @Summary("Field")
    @DisplayName("Field")
    @Expression(ExpressionSupport.NOT_SUPPORTED)
    private SearchPlaceField searchPlaceField;

    public SearchPlaceField getSearchPlaceField() {
        return searchPlaceField;
    }
}
