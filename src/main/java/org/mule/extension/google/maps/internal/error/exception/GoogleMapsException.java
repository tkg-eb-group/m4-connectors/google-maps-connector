/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.internal.error.exception;

import org.mule.runtime.extension.api.error.ErrorTypeDefinition;
import org.mule.runtime.extension.api.exception.ModuleException;
import org.mule.runtime.http.api.HttpConstants;

public final class GoogleMapsException extends ModuleException {

    private final int statusCode;

    public <T extends Enum<T>> GoogleMapsException(String message, ErrorTypeDefinition<T> errorTypeDefinition, HttpConstants.HttpStatus status) {
        super(message, errorTypeDefinition);
        this.statusCode = status.getStatusCode();
    }

    public int getStatusCode() {
        return statusCode;
    }
}
