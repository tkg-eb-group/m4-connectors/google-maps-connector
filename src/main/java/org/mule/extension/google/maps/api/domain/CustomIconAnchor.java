/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.api.domain;

public enum CustomIconAnchor {

    TOP("top"),
    BOTTOM("bottom"),
    LEFT("left"),
    RIGHT("right"),
    CENTER("center"),
    TOP_LEFT("topleft"),
    TOP_RIGHT("topright"),
    BOTTOM_LEFT("bottomleft"),
    BOTTOM_RIGHT("bottomright");

    private final String anchor;

    CustomIconAnchor(String anchor) {
        this.anchor = anchor;
    }

    public String getAnchor() {
        return anchor;
    }

}
