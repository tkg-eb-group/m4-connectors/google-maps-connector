/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.internal.operation;

import org.mule.extension.google.maps.api.domain.ImageFormat;
import org.mule.extension.google.maps.api.domain.MapType;
import org.mule.extension.google.maps.api.domain.provider.LanguageValueProvider;
import org.mule.extension.google.maps.api.domain.provider.RegionValueProvider;
import org.mule.extension.google.maps.api.param.MapSizeParameter;
import org.mule.extension.google.maps.api.param.MapsGeneralParameter;
import org.mule.extension.google.maps.internal.connection.GoogleMapsConnection;
import org.mule.extension.google.maps.internal.error.provider.GeneralErrorsProvider;
import org.mule.extension.google.maps.internal.service.StaticMapsService;
import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.dsl.xml.ParameterDsl;
import org.mule.runtime.extension.api.annotation.error.Throws;
import org.mule.runtime.extension.api.annotation.param.Connection;
import org.mule.runtime.extension.api.annotation.param.MediaType;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.ParameterGroup;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Example;
import org.mule.runtime.extension.api.annotation.param.display.Placement;
import org.mule.runtime.extension.api.annotation.param.display.Summary;
import org.mule.runtime.extension.api.annotation.values.OfValues;
import org.mule.runtime.extension.api.runtime.operation.Result;

import java.io.InputStream;

import static org.mule.runtime.extension.api.annotation.param.display.Placement.ADVANCED_TAB;

public class StaticMapsOperations {

    @MediaType(value = "image/*")
    @Throws(GeneralErrorsProvider.class)
    @Summary("Service that creates your map based on URL parameters and returns the map as an image you can display on your web page")
    public Result<InputStream, Void> getStaticMap(@Connection GoogleMapsConnection googleMapsConnection,
                                                  @ParameterGroup(name = "Map Size") MapSizeParameter mapSizeParameter, @ParameterGroup(name = "General Parameters") MapsGeneralParameter mapsGeneralParameter,

                                                  @Summary("Affects the number of pixels that are returned")
                                                  @DisplayName("Scale")
                                                  @Example("1")
                                                  @Optional
                                                  @Placement(tab = ADVANCED_TAB)
                                                  @Expression(ExpressionSupport.SUPPORTED)
                                                  @ParameterDsl(allowReferences = false) Integer scale,


                                                  @Summary("Defines the format of the resulting image")
                                                  @DisplayName("Image Format")
                                                  @Optional
                                                  @Placement(tab = ADVANCED_TAB)
                                                  @Expression(ExpressionSupport.NOT_SUPPORTED)
                                                  @ParameterDsl(allowReferences = false) ImageFormat imageFormat,

                                                  @Summary("Defines the type of map to construct")
                                                  @DisplayName("Map Type")
                                                  @Optional
                                                  @Placement(tab = ADVANCED_TAB)
                                                  @Expression(ExpressionSupport.NOT_SUPPORTED)
                                                  @ParameterDsl(allowReferences = false) MapType mapType,

                                                  @OfValues(LanguageValueProvider.class)
                                                  @Summary("The language in which to return results")
                                                  @DisplayName("Language")
                                                  @Optional
                                                  @Placement(tab = ADVANCED_TAB)
                                                  @Expression(ExpressionSupport.SUPPORTED)
                                                  @ParameterDsl(allowReferences = false) String language,

                                                  @OfValues(RegionValueProvider.class)
                                                  @Summary("Specifies the region code, specified as a ccTLD (\"top-level domain\") two-character value")
                                                  @DisplayName("Region Biasing")
                                                  @Optional
                                                  @Placement(tab = ADVANCED_TAB)
                                                  @Expression(ExpressionSupport.SUPPORTED)
                                                  @ParameterDsl(allowReferences = false) String region) {
        return new StaticMapsService().getStaticMap(googleMapsConnection.getGeoApiContext(), mapSizeParameter, mapsGeneralParameter,
                scale, imageFormat, mapType, language, region);
    }
}
