/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.internal.operation;

import com.google.gson.Gson;
import com.google.maps.model.SnappedPoint;
import com.google.maps.model.SpeedLimit;
import org.mule.extension.google.maps.api.param.*;
import org.mule.extension.google.maps.internal.connection.GoogleMapsConnection;
import org.mule.extension.google.maps.internal.error.provider.GeneralErrorsProvider;
import org.mule.extension.google.maps.internal.service.RoadsService;
import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.dsl.xml.ParameterDsl;
import org.mule.runtime.extension.api.annotation.error.Throws;
import org.mule.runtime.extension.api.annotation.metadata.fixed.OutputJsonType;
import org.mule.runtime.extension.api.annotation.param.*;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Placement;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

import static org.mule.runtime.extension.api.annotation.param.display.Placement.ADVANCED_TAB;

public class RoadsOperations {

    @MediaType(value = "application/json")
    @Throws(GeneralErrorsProvider.class)
    @OutputJsonType(schema = "schema/snap-to-roads-schema.json")
    @Summary("Service that returns the best-fit road geometry for a given set of GPS coordinates")
    public InputStream snapToRoadsRequest(@Connection GoogleMapsConnection googleMapsConnection,
                                          @ParameterGroup(name = "Paths") RoadsPathsParameter pathParameter,
                                          @Summary("Whether to interpolate a path to include all points forming the full road-geometry")
                                          @DisplayName("Interpolate")
                                          @Optional
                                          @Placement(tab = ADVANCED_TAB)
                                          @Expression(ExpressionSupport.SUPPORTED)
                                          @ParameterDsl(allowReferences = false) boolean isInterpolate) {
        SnappedPoint[] result = new RoadsService().snapToRoads(googleMapsConnection.getGeoApiContext(), pathParameter, isInterpolate);
        return new ByteArrayInputStream(new Gson().toJson(result).getBytes(StandardCharsets.UTF_8));
    }

    @MediaType(value = "application/json")
    @Throws(GeneralErrorsProvider.class)
    @OutputJsonType(schema = "schema/nearest-roads-schema.json")
    @Summary("Service that returns individual road segments for a given set of GPS coordinates")
    public InputStream nearestRoadsRequest(@Connection GoogleMapsConnection googleMapsConnection,
                                           @ParameterGroup(name = "Points") RoadsPointsParameter pointsParameter){
        SnappedPoint[] result = new RoadsService().nearestRoads(googleMapsConnection.getGeoApiContext(), pointsParameter);
        return new ByteArrayInputStream(new Gson().toJson(result).getBytes(StandardCharsets.UTF_8));
    }

    @MediaType(value = "application/json")
    @Throws(GeneralErrorsProvider.class)
    @OutputJsonType(schema = "schema/speed-limits-schema.json")
    @Summary("Service that returns the posted speed limit for a road segment")
    public InputStream speedLimitsRequest(@Connection GoogleMapsConnection googleMapsConnection,
                                   @ParameterGroup(name = "Speed Limits Parameters")
                                           SpeedLimitParameter speedLimitParameter){
        SpeedLimit[] result = new RoadsService().speedLimits(googleMapsConnection.getGeoApiContext(), speedLimitParameter);
        return new ByteArrayInputStream(new Gson().toJson(result).getBytes(StandardCharsets.UTF_8));
    }
}
