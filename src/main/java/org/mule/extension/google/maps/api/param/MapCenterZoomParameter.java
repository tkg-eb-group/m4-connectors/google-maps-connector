/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.api.param;

import org.mule.extension.google.maps.api.param.interfaces.MapsLocationParameter;
import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.dsl.xml.ParameterDsl;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

public class MapCenterZoomParameter {

    @Parameter
    @Summary("Defines the center of the map, equidistant from all edges of the map")
    @DisplayName("Center")
    @Expression(ExpressionSupport.NOT_SUPPORTED)
    @ParameterDsl(allowReferences = false)
    private MapsLocationParameter center;

    @Parameter
    @Summary("Defines the zoom level of the map, which determines the magnification level of the map")
    @DisplayName("Zoom")
    @Expression(ExpressionSupport.SUPPORTED)
    @ParameterDsl(allowReferences = false)
    private Integer zoom;

    public MapsLocationParameter getCenter() {
        return center;
    }

    public Integer getZoom() {
        return zoom;
    }

}
