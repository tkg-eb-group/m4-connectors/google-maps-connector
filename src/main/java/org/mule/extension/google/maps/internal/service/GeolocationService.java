/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.internal.service;

import com.google.maps.GeoApiContext;
import com.google.maps.GeolocationApi;
import com.google.maps.errors.ApiException;
import com.google.maps.model.CellTower;
import com.google.maps.model.GeolocationPayload;
import com.google.maps.model.GeolocationResult;
import com.google.maps.model.WifiAccessPoint;
import org.mule.extension.google.maps.api.param.request.GeolocationRequest;
import org.mule.extension.google.maps.internal.util.ExceptionUtil;

import java.io.IOException;

public class GeolocationService {

    public GeolocationResult getGeolocation(GeoApiContext context, GeolocationRequest geolocationRequestBody){
        GeolocationPayload.GeolocationPayloadBuilder geolocationPayloadBuilder = new GeolocationPayload.GeolocationPayloadBuilder();
        setGeolocationParameters(geolocationRequestBody, geolocationPayloadBuilder);
        setCellTowersParameter(geolocationRequestBody, geolocationPayloadBuilder);
        setWifiAccessPointParameter(geolocationRequestBody, geolocationPayloadBuilder);
        try {
            return GeolocationApi.geolocate(context, geolocationPayloadBuilder.createGeolocationPayload()).await();
        } catch (ApiException | IOException ex) {
            throw new ExceptionUtil().throwGoogleMapsException(ex);
        } catch (InterruptedException ex){
            Thread.currentThread().interrupt();
            throw new ExceptionUtil().throwGoogleMapsException(ex);
        }
    }

    private void setWifiAccessPointParameter(GeolocationRequest geolocationRequestBody, GeolocationPayload.GeolocationPayloadBuilder geolocationPayloadBuilder) {
        if(geolocationRequestBody.getWifiAccessPoints() != null && !geolocationRequestBody.getWifiAccessPoints().isEmpty()){
            WifiAccessPoint.WifiAccessPointBuilder wifiAccessPointBuilder = new WifiAccessPoint.WifiAccessPointBuilder();
            WifiAccessPoint[] wifiAccessPoints = geolocationRequestBody.getWifiAccessPoints().stream()
                    .map(wiFiAccessPointObjectsRequest -> wifiAccessPointBuilder.MacAddress(wiFiAccessPointObjectsRequest.getMacAddress())
                    .SignalStrength(wiFiAccessPointObjectsRequest.getSignalStrength()).Age(wiFiAccessPointObjectsRequest.getAge())
                            .Channel(wiFiAccessPointObjectsRequest.getChannel())
                            .SignalToNoiseRatio(wiFiAccessPointObjectsRequest.getSignalToNoiseRatio())
                            .createWifiAccessPoint()).toArray(WifiAccessPoint[]::new);
            geolocationPayloadBuilder.WifiAccessPoints(wifiAccessPoints);
        }
    }

    private void setCellTowersParameter(GeolocationRequest geolocationRequestBody, GeolocationPayload.GeolocationPayloadBuilder geolocationPayloadBuilder) {
        if(geolocationRequestBody.getCellTowers() != null && !geolocationRequestBody.getCellTowers().isEmpty()){
            CellTower.CellTowerBuilder cellTowerBuilder = new CellTower.CellTowerBuilder();
            CellTower[] cellTowers = geolocationRequestBody.getCellTowers().stream()
                    .map(cellTowerObjectsRequest -> cellTowerBuilder.CellId(cellTowerObjectsRequest.getCellId())
                    .LocationAreaCode(cellTowerObjectsRequest.getLocationAreaCode())
                            .MobileCountryCode(cellTowerObjectsRequest.getMobileCountryCode())
                    .MobileNetworkCode(cellTowerObjectsRequest.getMobileNetworkCode())
                            .Age(cellTowerObjectsRequest.getAge())
                    .SignalStrength(cellTowerObjectsRequest.getSignalStrength())
                    .TimingAdvance(cellTowerObjectsRequest.getTimingAdvance()).createCellTower()).toArray(CellTower[]::new);
            geolocationPayloadBuilder.CellTowers(cellTowers);
        }
    }

    private void setGeolocationParameters(GeolocationRequest geolocationRequestBody, GeolocationPayload.GeolocationPayloadBuilder geolocationPayloadBuilder) {
        if(geolocationRequestBody.getHomeMobileCountryCode() != null){
            geolocationPayloadBuilder.HomeMobileCountryCode(geolocationRequestBody.getHomeMobileCountryCode());
        }
        if(geolocationRequestBody.getHomeMobileNetworkCode() != null){
            geolocationPayloadBuilder.HomeMobileNetworkCode(geolocationRequestBody.getHomeMobileNetworkCode());
        }
        if(geolocationRequestBody.getRadioType() != null){
            geolocationPayloadBuilder.RadioType(geolocationRequestBody.getRadioType().getType());
        }
        if(geolocationRequestBody.getCarrier() != null && !geolocationRequestBody.getCarrier().isEmpty()){
            geolocationPayloadBuilder.Carrier(geolocationRequestBody.getCarrier());
        }
        if(geolocationRequestBody.isConsiderIp()){
            geolocationPayloadBuilder.ConsiderIp(geolocationRequestBody.isConsiderIp());
        }
    }
}
