/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.api.domain;

public enum AddressType {

    STREET_ADDRESS("street_address"), ROUTE("route"), INTERSECTION("intersection"),
    POLITICAL("political"), COUNTRY("country"), ADMINISTRATIVE_AREA_LEVEL_1("administrative_area_level_1"),
    ADMINISTRATIVE_AREA_LEVEL_2("administrative_area_level_2"),ADMINISTRATIVE_AREA_LEVEL_3("administrative_area_level_3"),
    ADMINISTRATIVE_AREA_LEVEL_4("administrative_area_level_4"),ADMINISTRATIVE_AREA_LEVEL_5("administrative_area_level_5"),
    COLLOQUIAL_AREA("colloquial_area"), LOCALITY("locality"), SUBLOCALITY("sublocality"),
    NEIGHBORHOOD("neighborhood"), PREMISE("premise"), SUBPREMISE("subpremise"),
    PLUS_CODE("plus_code"), POSTAL_CODE("postal_code"), NATURAL_FEATURE("natural_feature"),
    AIRPORT("airport"), PARK("park"), POINT_OF_INTEREST("point_of_interest");

    private final String type;

    AddressType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
