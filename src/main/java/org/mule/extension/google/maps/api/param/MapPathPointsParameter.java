/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.api.param;

import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Alias;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.dsl.xml.ParameterDsl;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Example;
import org.mule.runtime.extension.api.annotation.param.display.Placement;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

import static org.mule.runtime.extension.api.annotation.param.display.Placement.ADVANCED_TAB;

@Alias("Map Path")
public class MapPathPointsParameter {

    @Parameter
    @Summary("Specifies the thickness of the path in pixels")
    @DisplayName("Weight")
    @Example("5")
    @Optional
    @Placement(tab = ADVANCED_TAB)
    @Expression(ExpressionSupport.SUPPORTED)
    @ParameterDsl(allowReferences = false)
    private Integer weight;

    @Parameter
    @Summary("Specifies a color")
    @DisplayName("Color")
    @Example("0xFFFFCCFF | red")
    @Optional
    @Placement(tab = ADVANCED_TAB)
    @Expression(ExpressionSupport.SUPPORTED)
    @ParameterDsl(allowReferences = false)
    private String color;

    @Parameter
    @Summary("Indicates both that the path marks off a polygonal area and specifies the fill color to use as an overlay within that area")
    @DisplayName("Fill Color")
    @Example("0xFFFFCCFF | red")
    @Optional
    @Placement(tab = ADVANCED_TAB)
    @Expression(ExpressionSupport.SUPPORTED)
    @ParameterDsl(allowReferences = false)
    private String fillColor;

    @Parameter
    @Summary("Indicates that the requested path should be interpreted as a geodesic line that follows the curvature of the earth")
    @DisplayName("Geodesic")
    @Optional
    @Placement(tab = ADVANCED_TAB)
    @Expression(ExpressionSupport.SUPPORTED)
    @ParameterDsl(allowReferences = false)
    private boolean geodesic;

    @Parameter
    @Summary("Path Points")
    @DisplayName("Path Points")
    @Optional
    @Placement(tab = ADVANCED_TAB)
    @Expression(ExpressionSupport.NOT_SUPPORTED)
    @ParameterDsl(allowReferences = false)
    private MapPointsParameter mapPointsParameter;

    public Integer getWeight() {
        return weight;
    }

    public String getColor() {
        return color;
    }

    public String getFillColor() {
        return fillColor;
    }

    public boolean isGeodesic() {
        return geodesic;
    }

    public MapPointsParameter getMapPointsParameter() {
        return mapPointsParameter;
    }
}
