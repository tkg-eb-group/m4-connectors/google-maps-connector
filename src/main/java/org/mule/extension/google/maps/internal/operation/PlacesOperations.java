/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.internal.operation;

import com.google.gson.Gson;
import com.google.maps.model.AutocompletePrediction;
import com.google.maps.model.FindPlaceFromText;
import com.google.maps.model.PlaceDetails;
import com.google.maps.model.PlacesSearchResponse;
import org.mule.extension.google.maps.api.domain.PlaceAutocompleteType;
import org.mule.extension.google.maps.api.domain.PlaceType;
import org.mule.extension.google.maps.api.domain.PriceLevel;
import org.mule.extension.google.maps.api.domain.RankBy;
import org.mule.extension.google.maps.api.domain.provider.LanguageValueProvider;
import org.mule.extension.google.maps.api.domain.provider.RegionValueProvider;
import org.mule.extension.google.maps.api.param.*;
import org.mule.extension.google.maps.api.param.interfaces.InputTypeParameter;
import org.mule.extension.google.maps.api.param.interfaces.LocationBiasParameter;
import org.mule.extension.google.maps.internal.connection.GoogleMapsConnection;
import org.mule.extension.google.maps.internal.error.provider.GeneralErrorsProvider;
import org.mule.extension.google.maps.internal.error.provider.PlaceDetailsErrorsProvider;
import org.mule.extension.google.maps.internal.error.provider.PlacePhotosErrorsProvider;
import org.mule.extension.google.maps.internal.service.PlacesService;
import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Alias;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.dsl.xml.ParameterDsl;
import org.mule.runtime.extension.api.annotation.error.Throws;
import org.mule.runtime.extension.api.annotation.metadata.fixed.OutputJsonType;
import org.mule.runtime.extension.api.annotation.param.*;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Example;
import org.mule.runtime.extension.api.annotation.param.display.Placement;
import org.mule.runtime.extension.api.annotation.param.display.Summary;
import org.mule.runtime.extension.api.annotation.values.OfValues;
import org.mule.runtime.extension.api.runtime.operation.Result;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Set;

import static org.mule.runtime.extension.api.annotation.param.display.Placement.ADVANCED_TAB;

public class PlacesOperations {

    @MediaType(value = "application/json")
    @Throws(GeneralErrorsProvider.class)
    @OutputJsonType(schema = "schema/find-place-schema.json")
    @Summary("Service that takes a text input and returns a place")
    public InputStream findPlaceRequest(@Connection GoogleMapsConnection googleMapsConnection,
                                        @Summary("The type of input") @DisplayName("Input Type")
                                        @Expression(ExpressionSupport.NOT_SUPPORTED)
                                        @ParameterDsl(allowReferences = false) InputTypeParameter inputType,
                                        @OfValues(LanguageValueProvider.class)
                                        @Summary("The language in which to return results")
                                        @DisplayName("Language")
                                        @Optional
                                        @Placement(tab = ADVANCED_TAB)
                                        @Expression(ExpressionSupport.SUPPORTED)
                                        @ParameterDsl(allowReferences = false) String language,

                                        @Summary("The fields specifying the types of place data to return")
                                        @DisplayName("Fields")
                                        @Optional
                                        @NullSafe
                                        @Placement(tab = ADVANCED_TAB)
                                        @Expression(ExpressionSupport.NOT_SUPPORTED)
                                        @ParameterDsl(allowReferences = false) Set<SearchPlaceFieldsParameter> placesFields,

                                        @Summary("Prefer results in a specified area")
                                        @DisplayName("Location Bias")
                                        @Optional
                                        @Placement(tab = ADVANCED_TAB)
                                        @Expression(ExpressionSupport.NOT_SUPPORTED)
                                        @ParameterDsl(allowReferences = false) LocationBiasParameter locationBias) {
        FindPlaceFromText result = new PlacesService().findPlace(googleMapsConnection.getGeoApiContext(), inputType,
                language, placesFields, locationBias);
        return new ByteArrayInputStream(new Gson().toJson(result).getBytes(StandardCharsets.UTF_8));
    }

    @MediaType(value = "application/json")
    @Throws(GeneralErrorsProvider.class)
    @OutputJsonType(schema = "schema/nearby-search-schema.json")
    @Summary("Service that lets you search for places within a specified area")
    public InputStream nearbySearchRequest(@Connection GoogleMapsConnection googleMapsConnection,
                                           @ParameterGroup(name = "Location Parameters") SearchLocationParameter latitudeLongitudeParameter,
                                           @Summary("A term to be matched against all content that Google has indexed for this place")
                                           @DisplayName("Keyword")
                                           @Optional
                                           @Placement(tab = ADVANCED_TAB)
                                           @Expression(ExpressionSupport.SUPPORTED)
                                           @ParameterDsl(allowReferences = false) String keyword,

                                           @OfValues(LanguageValueProvider.class)
                                           @Summary("The language in which to return results")
                                           @DisplayName("Language")
                                           @Optional
                                           @Placement(tab = ADVANCED_TAB)
                                           @Expression(ExpressionSupport.SUPPORTED)
                                           @ParameterDsl(allowReferences = false) String language,

                                           @Summary("Restricts results to only those places within the specified range")
                                           @DisplayName("Min price")
                                           @Optional
                                           @Placement(tab = ADVANCED_TAB)
                                           @Expression(ExpressionSupport.NOT_SUPPORTED)
                                           @ParameterDsl(allowReferences = false) PriceLevel minPrice,

                                           @Summary("Restricts results to only those places within the specified range")
                                           @DisplayName("Max price")
                                           @Optional
                                           @Placement(tab = ADVANCED_TAB)
                                           @Expression(ExpressionSupport.NOT_SUPPORTED)
                                           @ParameterDsl(allowReferences = false) PriceLevel maxPrice,

                                           @Summary("A term to be matched against all content that Google has indexed for this place")
                                           @DisplayName("Name")
                                           @Optional
                                           @Placement(tab = ADVANCED_TAB)
                                           @Expression(ExpressionSupport.SUPPORTED)
                                           @ParameterDsl(allowReferences = false) String indexedName,

                                           @Summary("The places that are open for business at the time the query is sent")
                                           @DisplayName("Open Now")
                                           @Optional
                                           @Placement(tab = ADVANCED_TAB)
                                           @Expression(ExpressionSupport.SUPPORTED)
                                           @ParameterDsl(allowReferences = false) boolean openNow,

                                           @Summary("Specifies the order in which results are listed")
                                           @DisplayName("Rank By")
                                           @Optional
                                           @Placement(tab = ADVANCED_TAB)
                                           @Expression(ExpressionSupport.NOT_SUPPORTED)
                                           @ParameterDsl(allowReferences = false) RankBy rankBy,

                                           @Summary("Restricts the results to places matching the specified type")
                                           @DisplayName("Place Type")
                                           @Optional
                                           @Placement(tab = ADVANCED_TAB)
                                           @Expression(ExpressionSupport.NOT_SUPPORTED)
                                           @ParameterDsl(allowReferences = false) PlaceType placeType,

                                           @Summary("Returns up to 20 results from a previously run search")
                                           @DisplayName("Page Token")
                                           @Optional
                                           @Placement(tab = ADVANCED_TAB)
                                           @Expression(ExpressionSupport.SUPPORTED)
                                           @ParameterDsl(allowReferences = false) String pageToken) {
        PlacesSearchResponse result = new PlacesService().nearbySearch(googleMapsConnection.getGeoApiContext(),
                latitudeLongitudeParameter, keyword, language, minPrice, maxPrice, indexedName, openNow,
                rankBy, placeType, pageToken);
        return new ByteArrayInputStream(new Gson().toJson(result).getBytes(StandardCharsets.UTF_8));
    }

    @MediaType(value = "application/json")
    @Throws(GeneralErrorsProvider.class)
    @OutputJsonType(schema = "schema/text-search-schema.json")
    @Summary("Service that returns information about a set of places based on a string")
    public InputStream textSearchRequest(@Connection GoogleMapsConnection googleMapsConnection,
                                         @Summary("The text string on which to search") @DisplayName("Query")
                                         @Example("123 Main Street") @Expression(ExpressionSupport.SUPPORTED)
                                         @ParameterDsl(allowReferences = false) String query,

                                         @Summary("The latitude/longitude and radius around which to retrieve place information")
                                         @DisplayName("Search Location")
                                         @Optional
                                         @Placement(tab = ADVANCED_TAB)
                                         @Expression(ExpressionSupport.NOT_SUPPORTED)
                                         @ParameterDsl(allowReferences = false) TextSearchLocationParameter textSearchLocation,

                                         @OfValues(RegionValueProvider.class)
                                         @Summary("The region code, specified as a ccTLD (country code top-level domain) two-character value")
                                         @DisplayName("Region")
                                         @Optional
                                         @Placement(tab = ADVANCED_TAB)
                                         @Expression(ExpressionSupport.SUPPORTED)
                                         @ParameterDsl(allowReferences = false) String region,

                                         @OfValues(LanguageValueProvider.class)
                                         @Summary("The language in which to return results")
                                         @DisplayName("Language")
                                         @Optional
                                         @Placement(tab = ADVANCED_TAB)
                                         @Expression(ExpressionSupport.SUPPORTED)
                                         @ParameterDsl(allowReferences = false) String language,

                                         @Summary("Restricts results to only those places within the specified range")
                                         @DisplayName("Min price")
                                         @Optional
                                         @Placement(tab = ADVANCED_TAB)
                                         @Expression(ExpressionSupport.NOT_SUPPORTED)
                                         @ParameterDsl(allowReferences = false) PriceLevel minPrice,

                                         @Summary("Restricts results to only those places within the specified range")
                                         @DisplayName("Max price")
                                         @Optional
                                         @Placement(tab = ADVANCED_TAB)
                                         @Expression(ExpressionSupport.NOT_SUPPORTED)
                                         @ParameterDsl(allowReferences = false) PriceLevel maxPrice,

                                         @Summary("The places that are open for business at the time the query is sent")
                                         @DisplayName("Open Now")
                                         @Optional
                                         @Placement(tab = ADVANCED_TAB)
                                         @Expression(ExpressionSupport.SUPPORTED)
                                         @ParameterDsl(allowReferences = false) boolean openNow,

                                         @Summary("Returns up to 20 results from a previously run search")
                                         @DisplayName("Page Token")
                                         @Optional
                                         @Placement(tab = ADVANCED_TAB)
                                         @Expression(ExpressionSupport.SUPPORTED)
                                         @ParameterDsl(allowReferences = false) String pageToken,

                                         @Summary("Restricts the results to places matching the specified type")
                                         @DisplayName("Place Type")
                                         @Optional
                                         @Placement(tab = ADVANCED_TAB)
                                         @Expression(ExpressionSupport.NOT_SUPPORTED)
                                         @ParameterDsl(allowReferences = false) PlaceType placeType) {
        PlacesSearchResponse result = new PlacesService().textSearch(googleMapsConnection.getGeoApiContext(), query,
                textSearchLocation, region, language, minPrice, maxPrice, openNow, pageToken, placeType);
        return new ByteArrayInputStream(new Gson().toJson(result).getBytes(StandardCharsets.UTF_8));
    }

    @MediaType(value = "application/json")
    @Throws(PlaceDetailsErrorsProvider.class)
    @OutputJsonType(schema = "schema/place-details-schema.json")
    @Summary("Service that returns more details about a particular establishment or point of interest")
    public InputStream placeDetailsRequest(@Connection GoogleMapsConnection googleMapsConnection,
                                           @Summary("Place Identifier") @DisplayName("Place ID")
                                           @Example("ChIJ3S-JXmauEmsRUcIaWtf4MzE") @Expression(ExpressionSupport.SUPPORTED)
                                           @ParameterDsl(allowReferences = false) String placeId,

                                           @OfValues(LanguageValueProvider.class)
                                           @Summary("The language in which to return results")
                                           @DisplayName("Language")
                                           @Optional
                                           @Placement(tab = ADVANCED_TAB)
                                           @Expression(ExpressionSupport.SUPPORTED)
                                           @ParameterDsl(allowReferences = false) String language,

                                           @OfValues(RegionValueProvider.class)
                                           @Summary("The region code, specified as a ccTLD (country code top-level domain) two-character value")
                                           @DisplayName("Region")
                                           @Optional
                                           @Placement(tab = ADVANCED_TAB)
                                           @Expression(ExpressionSupport.SUPPORTED)
                                           @ParameterDsl(allowReferences = false) String region,

                                           @Summary("A random string which identifies an autocomplete session for billing purposes")
                                           @DisplayName("Session Token")
                                           @Optional
                                           @Placement(tab = ADVANCED_TAB)
                                           @Expression(ExpressionSupport.SUPPORTED)
                                           @ParameterDsl(allowReferences = false) String sessionToken,

                                           @Summary("The fields specifying the types of place data to return")
                                           @DisplayName("Fields")
                                           @Optional
                                           @NullSafe
                                           @Placement(tab = ADVANCED_TAB)
                                           @Expression(ExpressionSupport.NOT_SUPPORTED)
                                           @ParameterDsl(allowReferences = false) Set<PlaceDetailsFieldsParameter> placesDetailsFields) {
        PlaceDetails result = new PlacesService().placeDetails(googleMapsConnection.getGeoApiContext(), placeId, placeId, region, sessionToken, placesDetailsFields);
        return new ByteArrayInputStream(new Gson().toJson(result).getBytes(StandardCharsets.UTF_8));
    }

    @MediaType(value = "application/json")
    @Throws(PlacePhotosErrorsProvider.class)
    @Summary("Service that gives you access to the millions of photos stored in the Places database")
    public Result<InputStream, Void> photosRequest(@Connection GoogleMapsConnection googleMapsConnection,
                                     @ParameterGroup(name = "Photo Request")
                                     @Expression(ExpressionSupport.NOT_SUPPORTED)
                                     @ParameterDsl(allowReferences = false) PhotoPropertiesParameter photoPropertiesParameter){
        return new PlacesService().photosRequest(googleMapsConnection.getGeoApiContext(), photoPropertiesParameter);
    }

    @MediaType(value = "application/json")
    @Throws(GeneralErrorsProvider.class)
    @OutputJsonType(schema = "schema/place-autocomplete-schema.json")
    @Summary("Service that returns place predictions in response")
    public InputStream placeAutocompleteRequest(@Connection GoogleMapsConnection googleMapsConnection,
                                                @Summary("The text string on which to search") @DisplayName("Input")
                                                @Example("1600 Amphitheatre") @Expression(ExpressionSupport.SUPPORTED)
                                                @ParameterDsl(allowReferences = false) String input,

                                                @Summary("A random string which identifies an autocomplete session for billing purposes")
                                                @DisplayName("Session Token")
                                                @Optional
                                                @Placement(tab = ADVANCED_TAB)
                                                @Expression(ExpressionSupport.SUPPORTED)
                                                @ParameterDsl(allowReferences = false) String sessionToken,

                                                @Summary("The position, in the input term, of the last character that the service uses to match predictions") @DisplayName("Offset")
                                                @Optional
                                                @Placement(tab = ADVANCED_TAB)
                                                @Example("3")
                                                @Expression(ExpressionSupport.SUPPORTED)
                                                @ParameterDsl(allowReferences = false)
                                                        Integer offset,

                                                @Summary("The origin point from which to calculate straight-line distance to the destination")
                                                @DisplayName("Origin")
                                                @Alias("PlaceOrigin")
                                                @Optional
                                                @Placement(tab = ADVANCED_TAB)
                                                @Expression(ExpressionSupport.NOT_SUPPORTED)
                                                @ParameterDsl(allowReferences = false) LatitudeLongitudeParameter origin,

                                                @Summary("The point around which you wish to retrieve place information")
                                                @DisplayName("Location")
                                                @Alias("PlaceLocation")
                                                @Optional
                                                @Placement(tab = ADVANCED_TAB)
                                                @Expression(ExpressionSupport.NOT_SUPPORTED)
                                                @ParameterDsl(allowReferences = false) SearchLocationParameter location,

                                                @Summary("Returns only those places that are strictly within the region defined by location and radius")
                                                @DisplayName("Strict Bounds")
                                                @Optional
                                                @Placement(tab = ADVANCED_TAB)
                                                @Expression(ExpressionSupport.SUPPORTED)
                                                @ParameterDsl(allowReferences = false) boolean isStrictBounds,

                                                @OfValues(LanguageValueProvider.class)
                                                @Summary("The language in which to return results")
                                                @DisplayName("Language")
                                                @Optional
                                                @Placement(tab = ADVANCED_TAB)
                                                @Expression(ExpressionSupport.SUPPORTED)
                                                @ParameterDsl(allowReferences = false)
                                                        String language,

                                                @Summary("Restricts the results to places matching the specified type")
                                                @DisplayName("Place Type")
                                                @Optional
                                                @Placement(tab = ADVANCED_TAB)
                                                @Expression(ExpressionSupport.NOT_SUPPORTED)
                                                @ParameterDsl(allowReferences = false) PlaceAutocompleteType placeType,

                                                @Summary("A grouping of places to which you would like to restrict your results")
                                                @DisplayName("Components")
                                                @Optional
                                                @Placement(tab = ADVANCED_TAB)
                                                @NullSafe
                                                @Expression(ExpressionSupport.NOT_SUPPORTED)
                                                @ParameterDsl(allowReferences = false) List<ComponentsParameter> components) {
        AutocompletePrediction[] result = new PlacesService().placeAutocomplete(googleMapsConnection.getGeoApiContext(), input, sessionToken, offset,
                origin, location, isStrictBounds, language, placeType, components);
        return new ByteArrayInputStream(new Gson().toJson(result).getBytes(StandardCharsets.UTF_8));
    }

    @MediaType(value = "application/json")
    @Throws(GeneralErrorsProvider.class)
    @OutputJsonType(schema = "schema/query-autocomplete-schema.json")
    @Summary("Service that can be used to provide a query prediction for text-based geographic searches, by returning suggested queries as you type")
    public InputStream queryAutocompleteRequest(@Connection GoogleMapsConnection googleMapsConnection,
                                                @Summary("The text string on which to search") @DisplayName("Input")
                                                @Example("Pizza near Paris") @Expression(ExpressionSupport.SUPPORTED)
                                                @ParameterDsl(allowReferences = false) String input,

                                                @Summary("The position, in the input term, of the last character that the service uses to match predictions") @DisplayName("Offset")
                                                @Optional
                                                @Placement(tab = ADVANCED_TAB)
                                                @Example("3")
                                                @Expression(ExpressionSupport.SUPPORTED)
                                                @ParameterDsl(allowReferences = false) Integer offset,

                                                @Summary("The point around which you wish to retrieve place information")
                                                @DisplayName("Location")
                                                @Alias("PlaceLocation")
                                                @Optional
                                                @Placement(tab = ADVANCED_TAB)
                                                @Expression(ExpressionSupport.NOT_SUPPORTED)
                                                @ParameterDsl(allowReferences = false) SearchLocationParameter location,

                                                @OfValues(LanguageValueProvider.class)
                                                @Summary("The language in which to return results")
                                                @DisplayName("Language")
                                                @Optional
                                                @Placement(tab = ADVANCED_TAB)
                                                @Expression(ExpressionSupport.SUPPORTED)
                                                @ParameterDsl(allowReferences = false) String language) {
        AutocompletePrediction[] result = new PlacesService().queryAutocomplete(googleMapsConnection.getGeoApiContext(), input, offset,
                location, language);
        return new ByteArrayInputStream(new Gson().toJson(result).getBytes(StandardCharsets.UTF_8));
    }
}
