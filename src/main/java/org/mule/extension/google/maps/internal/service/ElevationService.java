/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.internal.service;

import com.google.maps.ElevationApi;
import com.google.maps.GeoApiContext;
import com.google.maps.errors.ApiException;
import com.google.maps.errors.InvalidRequestException;
import com.google.maps.model.ElevationResult;
import com.google.maps.model.EncodedPolyline;
import com.google.maps.model.LatLng;
import org.mule.extension.google.maps.api.param.*;
import org.mule.extension.google.maps.api.param.interfaces.CoordinateParameter;
import org.mule.extension.google.maps.api.param.interfaces.ElevationParameter;
import org.mule.extension.google.maps.internal.error.exception.GoogleMapsException;
import org.mule.extension.google.maps.internal.error.GoogleMapsErrorType;
import org.mule.extension.google.maps.internal.util.ExceptionUtil;
import org.mule.extension.google.maps.internal.util.GoogleConstantsUtil;
import org.mule.runtime.http.api.HttpConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class ElevationService {

    private static final Logger logger = LoggerFactory.getLogger(ElevationService.class);

    public ElevationResult[] getElevation(GeoApiContext context, ElevationParameter elevationParameter) {
        String type = elevationParameter.getType();
        if (GoogleConstantsUtil.POSITIONAL_REQUEST_TYPE.equals(type)) {
            PositionParameter positionParameter = (PositionParameter) elevationParameter;
            CoordinateParameter coordinateParameter = positionParameter.getLocations();
            if (coordinateParameter.getType().equals(GoogleConstantsUtil.REQUEST_LAT_LNG_TYPE)) {
                return setPositionalParameterCoordinate(context, positionParameter);
            } else {
                return setPositionalParameterPolyline(context, positionParameter);
            }
        } else if (GoogleConstantsUtil.SAMPLED_PATH_REQUEST_TYPE.equals(type)) {
            CoordinateParameter coordinateParameter;
            SampledPathParameter sampledPathParameter = (SampledPathParameter) elevationParameter;
            coordinateParameter = sampledPathParameter.getPaths();
            if (coordinateParameter.getType().equals(GoogleConstantsUtil.REQUEST_LAT_LNG_TYPE)) {
                return setSampledParameterCoordinate(context, sampledPathParameter);
            } else {
                return setSampledParameterPolyline(context, sampledPathParameter);
            }
        }
        return new ElevationResult[0];
    }

    private ElevationResult[] setPositionalParameterCoordinate(GeoApiContext context, PositionParameter positionParameter) {
        ElevationResult[] elevationResult;
        LatLngCoordinateParameter latLngCoordinateParameter = (LatLngCoordinateParameter) positionParameter.getLocations();
        LatLng[] latLonPoints = latLngCoordinateParameter.getLatitudeLongitudeCoordinates().stream()
                .map(l -> new LatLng(l.getLatitude(),
                        l.getLongitude())).toArray(LatLng[]::new);
        try {
            elevationResult = ElevationApi.getByPoints(context, latLonPoints).await();
            return elevationResult;
        } catch (ApiException | IOException e) {
            throw new ExceptionUtil().throwGoogleMapsException(e);
        } catch (InterruptedException ex){
            Thread.currentThread().interrupt();
            throw new ExceptionUtil().throwGoogleMapsException(ex);
        }
    }

    private ElevationResult[] setPositionalParameterPolyline(GeoApiContext context, PositionParameter positionParameter) {
        ElevationResult[] elevationResult;
        PolylineCoordinateParameter polylineCoordinateParameter = (PolylineCoordinateParameter) positionParameter.getLocations();
        try {
            elevationResult = ElevationApi.getByPoints(context, new EncodedPolyline(polylineCoordinateParameter.getPolylineCoordinates())).await();
            return elevationResult;
        } catch (InvalidRequestException ex) {
            logger.error("Invalid request: Invalid polyline coordinate", ex);
            throw new GoogleMapsException("Invalid polyline coordinate",
                    GoogleMapsErrorType.ILLEGAL_ARGUMENT_EXCEPTION, HttpConstants.HttpStatus.BAD_REQUEST);
        } catch (ApiException | IOException e) {
            throw new ExceptionUtil().throwGoogleMapsException(e);
        } catch (InterruptedException ex){
            Thread.currentThread().interrupt();
            throw new ExceptionUtil().throwGoogleMapsException(ex);
        }
    }

    private ElevationResult[] setSampledParameterCoordinate(GeoApiContext context, SampledPathParameter sampledPathParameter) {
        ElevationResult[] elevationResult;
        LatLngCoordinateParameter latLngCoordinateParameter = (LatLngCoordinateParameter) sampledPathParameter.getPaths();
        LatLng[] latLonPoints = latLngCoordinateParameter.getLatitudeLongitudeCoordinates().stream()
                .map(l -> new LatLng(l.getLatitude(),
                        l.getLongitude())).toArray(LatLng[]::new);
        try {
            elevationResult = ElevationApi.getByPath(context, sampledPathParameter.getSamples(), latLonPoints).await();
            return elevationResult;
        } catch (ApiException | IOException e) {
            throw new ExceptionUtil().throwGoogleMapsException(e);
        } catch (InterruptedException ex){
            Thread.currentThread().interrupt();
            throw new ExceptionUtil().throwGoogleMapsException(ex);
        }
    }

    private ElevationResult[] setSampledParameterPolyline(GeoApiContext context, SampledPathParameter sampledPathParameter) {
        ElevationResult[] elevationResult;
        PolylineCoordinateParameter polylineCoordinateParameter = (PolylineCoordinateParameter) sampledPathParameter.getPaths();
        try {
            elevationResult = ElevationApi.getByPath(context, sampledPathParameter.getSamples(), new EncodedPolyline(polylineCoordinateParameter.getPolylineCoordinates())).await();
            return elevationResult;
        } catch (InvalidRequestException ex) {
            logger.error("Invalid request: Invalid polyline coordinate", ex);
            throw new GoogleMapsException("Invalid polyline coordinate",
                    GoogleMapsErrorType.ILLEGAL_ARGUMENT_EXCEPTION, HttpConstants.HttpStatus.BAD_REQUEST);
        } catch (ApiException | IOException e) {
            throw new ExceptionUtil().throwGoogleMapsException(e);
        } catch (InterruptedException ex){
            Thread.currentThread().interrupt();
            throw new ExceptionUtil().throwGoogleMapsException(ex);
        }
    }
}
