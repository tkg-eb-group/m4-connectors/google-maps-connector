/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.api.param;

import org.mule.extension.google.maps.api.domain.TransitType;
import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Alias;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

@Alias("Transit Mode")
public class TransitModeParameter {

    @Parameter
    @Summary("Transit Mode")
    @DisplayName("Transit Mode")
    @Expression(ExpressionSupport.NOT_SUPPORTED)
    private TransitType transitMode;

    public TransitType getTransitMode() {
        return transitMode;
    }
}
