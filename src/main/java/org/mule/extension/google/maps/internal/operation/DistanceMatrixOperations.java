/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.internal.operation;

import com.google.gson.Gson;
import com.google.maps.model.DistanceMatrix;
import org.mule.extension.google.maps.api.domain.*;
import org.mule.extension.google.maps.api.domain.provider.LanguageValueProvider;
import org.mule.extension.google.maps.api.param.TransitModeParameter;
import org.mule.extension.google.maps.api.param.interfaces.DateTimeParameter;
import org.mule.extension.google.maps.api.param.interfaces.DistanceMatrixOriginDestinationParameter;
import org.mule.extension.google.maps.internal.connection.GoogleMapsConnection;
import org.mule.extension.google.maps.internal.error.provider.DistanceMatrixErrorsProvider;
import org.mule.extension.google.maps.internal.service.DistanceMatrixService;
import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.dsl.xml.ParameterDsl;
import org.mule.runtime.extension.api.annotation.error.Throws;
import org.mule.runtime.extension.api.annotation.metadata.fixed.OutputJsonType;
import org.mule.runtime.extension.api.annotation.param.Connection;
import org.mule.runtime.extension.api.annotation.param.MediaType;
import org.mule.runtime.extension.api.annotation.param.NullSafe;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Placement;
import org.mule.runtime.extension.api.annotation.param.display.Summary;
import org.mule.runtime.extension.api.annotation.values.OfValues;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Set;

import static org.mule.runtime.extension.api.annotation.param.display.Placement.ADVANCED_TAB;

public class DistanceMatrixOperations {

    @MediaType(value = "application/json")
    @Throws(DistanceMatrixErrorsProvider.class)
    @OutputJsonType(schema = "schema/distance-matrix-schema.json")
    @Summary("Service that provides travel distance and time for a matrix of origins and destinations")
    public InputStream distanceMatrixRequest(@Connection GoogleMapsConnection googleMapsConnection,
                                             @Summary("Origins") @DisplayName("Origins") @Expression(ExpressionSupport.NOT_SUPPORTED) @ParameterDsl(allowReferences = false) List<DistanceMatrixOriginDestinationParameter> origins,
                                             @Summary("Destinations") @DisplayName("Destinations") @Expression(ExpressionSupport.NOT_SUPPORTED) @ParameterDsl(allowReferences = false) List<DistanceMatrixOriginDestinationParameter> destinations,
                                             @Summary("Specifies the mode of transport to use when calculating directions")
                                             @DisplayName("Travel Mode") @Optional
                                             @Placement(tab = ADVANCED_TAB)
                                             @Expression(ExpressionSupport.NOT_SUPPORTED)
                                             @ParameterDsl(allowReferences = false) TravelType travelMode,

                                             @Summary("Indicates that the calculated route(s) should avoid the indicated features")
                                             @DisplayName("Avoid Feature")
                                             @Optional @Placement(tab = ADVANCED_TAB)
                                             @Expression(ExpressionSupport.NOT_SUPPORTED)
                                             @ParameterDsl(allowReferences = false) AvoidRestriction avoid,

                                             @OfValues(LanguageValueProvider.class)
                                             @Summary("The language in which to return results")
                                             @DisplayName("Language")
                                             @Optional
                                             @Placement(tab = ADVANCED_TAB)
                                             @Expression(ExpressionSupport.SUPPORTED)
                                             @ParameterDsl(allowReferences = false) String language,

                                             @Summary("Specifies the unit system to use when displaying results")
                                             @DisplayName("Unit System")
                                             @Optional
                                             @Placement(tab = ADVANCED_TAB)
                                             @Expression(ExpressionSupport.NOT_SUPPORTED)
                                             @ParameterDsl(allowReferences = false) UnitSystem unit,

                                             @Summary("Arrival or Departure Date")
                                             @DisplayName("Date Time")
                                             @Optional
                                             @Placement(tab = ADVANCED_TAB)
                                             @Expression(ExpressionSupport.NOT_SUPPORTED)
                                             @ParameterDsl(allowReferences = false) DateTimeParameter dateTime,

                                             @Summary("Specifies the assumptions to use when calculating time in traffic")
                                             @DisplayName("Traffic Model")
                                             @Optional
                                             @Placement(tab = ADVANCED_TAB)
                                             @Expression(ExpressionSupport.NOT_SUPPORTED)
                                             @ParameterDsl(allowReferences = false) TrafficType trafficModel,

                                             @Summary("Specifies one or more preferred modes of transit")
                                             @DisplayName("Transit Modes")
                                             @Optional
                                             @NullSafe
                                             @Placement(tab = ADVANCED_TAB)
                                             @Expression(ExpressionSupport.NOT_SUPPORTED)
                                             @ParameterDsl(allowReferences = false) Set<TransitModeParameter> transitModes,

                                             @Summary("Specifies preferences for transit routes")
                                             @DisplayName("Transit Routing Preference")
                                             @Optional
                                             @Placement(tab = ADVANCED_TAB)
                                             @Expression(ExpressionSupport.NOT_SUPPORTED)
                                             @ParameterDsl(allowReferences = false) TransitRoutingSetting transitRoutingPreference) {
        DistanceMatrix result = new DistanceMatrixService().getDistanceMatrix(googleMapsConnection.getGeoApiContext(), origins, destinations, travelMode, avoid, language,
                unit, dateTime, trafficModel, transitModes, transitRoutingPreference);
        return new ByteArrayInputStream(new Gson().toJson(result).getBytes(StandardCharsets.UTF_8));
    }
}
