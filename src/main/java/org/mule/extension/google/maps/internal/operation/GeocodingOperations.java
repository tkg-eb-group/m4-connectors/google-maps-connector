/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.internal.operation;

import com.google.gson.Gson;
import com.google.maps.model.GeocodingResult;
import org.mule.extension.google.maps.api.domain.provider.LanguageValueProvider;
import org.mule.extension.google.maps.api.domain.provider.RegionValueProvider;
import org.mule.extension.google.maps.api.param.*;
import org.mule.extension.google.maps.internal.connection.GoogleMapsConnection;
import org.mule.extension.google.maps.internal.error.provider.GeneralErrorsProvider;
import org.mule.extension.google.maps.internal.error.provider.GeocodingErrorsProvider;
import org.mule.extension.google.maps.internal.service.GeocodingService;
import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.dsl.xml.ParameterDsl;
import org.mule.runtime.extension.api.annotation.error.Throws;
import org.mule.runtime.extension.api.annotation.metadata.fixed.OutputJsonType;
import org.mule.runtime.extension.api.annotation.param.*;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Placement;
import org.mule.runtime.extension.api.annotation.param.display.Summary;
import org.mule.runtime.extension.api.annotation.values.OfValues;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Set;

import static org.mule.runtime.extension.api.annotation.param.display.Placement.ADVANCED_TAB;

public class GeocodingOperations {

    @MediaType(value = "application/json")
    @Throws(GeocodingErrorsProvider.class)
    @OutputJsonType(schema = "schema/geocoding-schema.json")
    @Summary("Service for converting addresses into geographic coordinates, which you can use to place markers on a map, or position the map")
    public InputStream geocodingRequest(@Connection GoogleMapsConnection googleMapsConnection,
                                        @ParameterGroup(name = "Latitude/Longitude Lookup") GeocodingParameter geocodingParameter,
                                        @Summary("Bounds")
                                        @DisplayName("Bounds")
                                        @Optional
                                        @Placement(tab = ADVANCED_TAB)
                                        @Expression(ExpressionSupport.NOT_SUPPORTED)
                                        @ParameterDsl(allowReferences = false) BoundsParameter bounds,

                                        @OfValues(LanguageValueProvider.class)
                                        @Summary("The language in which to return results")
                                        @DisplayName("Language")
                                        @Optional
                                        @Placement(tab = ADVANCED_TAB)
                                        @Expression(ExpressionSupport.SUPPORTED)
                                        @ParameterDsl(allowReferences = false) String language,

                                        @OfValues(RegionValueProvider.class)
                                        @Summary("Specifies the region code, specified as a ccTLD (\"top-level domain\") two-character value")
                                        @DisplayName("Region Biasing")
                                        @Optional
                                        @Placement(tab = ADVANCED_TAB)
                                        @Expression(ExpressionSupport.SUPPORTED)
                                        @ParameterDsl(allowReferences = false) String region) {
        GeocodingResult[] result = new GeocodingService().getGeocoding(googleMapsConnection.getGeoApiContext(), geocodingParameter, bounds, language, region);
        return new ByteArrayInputStream(new Gson().toJson(result).getBytes(StandardCharsets.UTF_8));
    }

    @MediaType(value = "application/json")
    @Throws(GeneralErrorsProvider.class)
    @OutputJsonType(schema = "schema/reverse-geocoding-schema.json")
    @Summary("Service for converting geographic coordinates into a human-readable address")
    public InputStream reverseGeocodingRequest(@Connection GoogleMapsConnection googleMapsConnection,
                                               @ParameterGroup(name = "Location") LatitudeLongitudeParameter latitudeLongitudeParameter,
                                               @Summary("Specifies one or more preferred address types")
                                               @DisplayName("Address Types")
                                               @Optional
                                               @NullSafe
                                               @Placement(tab = ADVANCED_TAB)
                                               @Expression(ExpressionSupport.NOT_SUPPORTED)
                                               @ParameterDsl(allowReferences = false) Set<AddressTypeParameter> addressesTypes,

                                               @Summary("Specifies one or more preferred locations types")
                                               @DisplayName("Location Types")
                                               @Optional
                                               @NullSafe
                                               @Placement(tab = ADVANCED_TAB)
                                               @Expression(ExpressionSupport.NOT_SUPPORTED)
                                               @ParameterDsl(allowReferences = false) Set<LocationTypeParameter> locationsTypes,

                                               @OfValues(LanguageValueProvider.class)
                                               @Summary("The language in which to return results")
                                               @DisplayName("Language")
                                               @Optional
                                               @Placement(tab = ADVANCED_TAB)
                                               @Expression(ExpressionSupport.SUPPORTED)
                                               @ParameterDsl(allowReferences = false) String language) {
        GeocodingResult[] result = new GeocodingService().getReverseGeocoding(googleMapsConnection.getGeoApiContext(), latitudeLongitudeParameter, addressesTypes, locationsTypes, language);
        return new ByteArrayInputStream(new Gson().toJson(result).getBytes(StandardCharsets.UTF_8));
    }

    @MediaType(value = "application/json")
    @Throws(GeocodingErrorsProvider.class)
    @OutputJsonType(schema = "schema/retrieve-address-by-place-id-schema.json")
    @Summary("Service to get an address from a given place id")
    public InputStream retrieveAddressByPlaceId(@Connection GoogleMapsConnection googleMapsConnection,
                                           @ParameterGroup(name = "Place") PlaceIdParameter placeIdentifierParameter){
        GeocodingResult[] result = new GeocodingService().getAddressByPlaceId(googleMapsConnection.getGeoApiContext(), placeIdentifierParameter);
        return new ByteArrayInputStream(new Gson().toJson(result).getBytes(StandardCharsets.UTF_8));
    }
}
