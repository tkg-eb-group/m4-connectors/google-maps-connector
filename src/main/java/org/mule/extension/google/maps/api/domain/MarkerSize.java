/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.api.domain;

public enum MarkerSize {

    TINY("tiny"), MID("mid"),
    SMALL("small"), NORMAL("normal");

    private final String size;

    MarkerSize(String size) {
        this.size = size;
    }

    public String getSize() {
        return size;
    }

}
