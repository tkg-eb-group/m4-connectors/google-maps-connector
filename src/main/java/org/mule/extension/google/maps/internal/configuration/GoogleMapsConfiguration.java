/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.internal.configuration;

import org.mule.extension.google.maps.internal.connection.provider.GoogleMapsConnectionProvider;
import org.mule.extension.google.maps.internal.operation.*;
import org.mule.runtime.extension.api.annotation.Operations;
import org.mule.runtime.extension.api.annotation.connectivity.ConnectionProviders;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;

@DisplayName("Configuration")
@Operations({DirectionsOperations.class, ElevationOperations.class, TimeZoneOperations.class,
        DistanceMatrixOperations.class, GeocodingOperations.class, GeolocationOperations.class,
        PlacesOperations.class, RoadsOperations.class, StaticMapsOperations.class})
@ConnectionProviders(GoogleMapsConnectionProvider.class)
public class GoogleMapsConfiguration {

}
