/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.internal.service;

import com.google.maps.GeoApiContext;
import com.google.maps.RoadsApi;
import com.google.maps.errors.ApiException;
import com.google.maps.model.LatLng;
import com.google.maps.model.SnappedPoint;
import com.google.maps.model.SpeedLimit;
import org.mule.extension.google.maps.api.param.*;
import org.mule.extension.google.maps.internal.error.exception.GoogleMapsException;
import org.mule.extension.google.maps.internal.error.GoogleMapsErrorType;
import org.mule.extension.google.maps.internal.util.ExceptionUtil;
import org.mule.runtime.http.api.HttpConstants;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class RoadsService {

    public SnappedPoint[] snapToRoads(GeoApiContext context, RoadsPathsParameter pathParameter,
                                      boolean isInterpolate){
        List<LatLng> latLngs = new ArrayList<>();
        if(pathParameter == null || pathParameter.getPathsLocations().isEmpty()){
            throw new GoogleMapsException("Path is required",
                    GoogleMapsErrorType.ILLEGAL_ARGUMENT_EXCEPTION, HttpConstants.HttpStatus.BAD_REQUEST);
        }
        setPathsLocations(latLngs, pathParameter.getPathsLocations());
        boolean interpolate = false;
        if (isInterpolate){
            interpolate = true;
        }
        try {
            return RoadsApi.snapToRoads(context, interpolate, latLngs.toArray(new LatLng[0])).await();
        } catch (ApiException | IOException ex) {
            throw new ExceptionUtil().throwGoogleMapsException(ex);
        } catch (InterruptedException ex){
            Thread.currentThread().interrupt();
            throw new ExceptionUtil().throwGoogleMapsException(ex);
        }
    }

    private void setPathsLocations(List<LatLng> latLngs, List<LatitudeLongitudeParameter> pathsLocations) {
        for (LatitudeLongitudeParameter path : pathsLocations) {
            latLngs.add(new LatLng(path.getLatitude(), path.getLongitude()));
        }
    }

    public SnappedPoint[] nearestRoads(GeoApiContext context, RoadsPointsParameter pointsParameter){
        List<LatLng> latLngs = new ArrayList<>();
        if(pointsParameter == null || pointsParameter.getPointsLocations().isEmpty()){
            throw new GoogleMapsException("Points are required",
                    GoogleMapsErrorType.ILLEGAL_ARGUMENT_EXCEPTION, HttpConstants.HttpStatus.BAD_REQUEST);
        }
        setPathsLocations(latLngs, pointsParameter.getPointsLocations());
        try {
            return RoadsApi.nearestRoads(context, latLngs.toArray(new LatLng[0])).await();
        } catch (ApiException | IOException ex) {
            throw new ExceptionUtil().throwGoogleMapsException(ex);
        } catch (InterruptedException ex){
            Thread.currentThread().interrupt();
            throw new ExceptionUtil().throwGoogleMapsException(ex);
        }
    }

    public SpeedLimit[] speedLimits(GeoApiContext context, SpeedLimitParameter speedLimitParameter){
        List<LatLng> latLngs = new ArrayList<>();
        List<String> placeIds = new ArrayList<>();
        if((speedLimitParameter.getPathsLocations() == null || speedLimitParameter.getPathsLocations()
                .getPathsLocations().isEmpty()) && (speedLimitParameter.getPlaceIdsLocation() == null
        || speedLimitParameter.getPlaceIdsLocation().getPlaceIds().isEmpty())){
            throw new GoogleMapsException("Path or place id is required",
                    GoogleMapsErrorType.ILLEGAL_ARGUMENT_EXCEPTION, HttpConstants.HttpStatus.BAD_REQUEST);
        }
        setSpeedLimitsParameters(speedLimitParameter, latLngs, placeIds);
        try {
            SpeedLimit[] result;
            if(!latLngs.isEmpty()){
                result = RoadsApi.speedLimits(context, latLngs.toArray(new LatLng[0])).await();
            } else {
                result = RoadsApi.speedLimits(context, placeIds.toArray(new String[0])).await();
            }
            return result;
        } catch (ApiException | IOException ex) {
            throw new ExceptionUtil().throwGoogleMapsException(ex);
        } catch (InterruptedException ex){
            Thread.currentThread().interrupt();
            throw new ExceptionUtil().throwGoogleMapsException(ex);
        }
    }

    private void setSpeedLimitsParameters(SpeedLimitParameter speedLimitParameter, List<LatLng> latLngs, List<String> placeIds) {
        if(speedLimitParameter.getPathsLocations() != null){
            setPathsLocations(latLngs, speedLimitParameter.getPathsLocations().getPathsLocations());
        }
        if(speedLimitParameter.getPlaceIdsLocation() != null){
            for(PlaceIdParameter placeIdentifierParameter : speedLimitParameter.getPlaceIdsLocation().getPlaceIds()){
                placeIds.add(placeIdentifierParameter.getPlaceId());
            }
        }
    }
}
