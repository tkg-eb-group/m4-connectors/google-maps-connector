/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.api.param;

import org.mule.extension.google.maps.api.param.interfaces.CoordinateParameter;
import org.mule.extension.google.maps.api.param.interfaces.ElevationParameter;
import org.mule.extension.google.maps.internal.util.GoogleConstantsUtil;
import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Alias;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.dsl.xml.ParameterDsl;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Example;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

@Alias("Sampled Path Request")
public class SampledPathParameter implements ElevationParameter {

    @Parameter
    @Summary("Path on earth from which to return elevation data")
    @DisplayName("Paths")
    @Expression(ExpressionSupport.NOT_SUPPORTED)
    @ParameterDsl(allowReferences = false)
    private CoordinateParameter paths;

    @Parameter
    @Summary("Number of sample points along a path for which to return elevation data")
    @DisplayName("Samples")
    @Example("2")
    @Expression(ExpressionSupport.NOT_SUPPORTED)
    @ParameterDsl(allowReferences = false)
    private Integer samples;

    public CoordinateParameter getPaths() {
        return paths;
    }

    public Integer getSamples() {
        return samples;
    }

    @Override
    public String getType() {
        return GoogleConstantsUtil.SAMPLED_PATH_REQUEST_TYPE;
    }
}
