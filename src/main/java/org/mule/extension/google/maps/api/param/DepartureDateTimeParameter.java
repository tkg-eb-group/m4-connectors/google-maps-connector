/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.api.param;

import org.mule.extension.google.maps.api.param.interfaces.DateTimeParameter;
import org.mule.extension.google.maps.internal.util.GoogleConstantsUtil;
import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Alias;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.dsl.xml.ParameterDsl;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Example;
import org.mule.runtime.extension.api.annotation.param.display.Placement;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

import java.time.LocalDateTime;

import static org.mule.runtime.extension.api.annotation.param.display.Placement.ADVANCED_TAB;

@Alias("Departure DateTime")
public class DepartureDateTimeParameter implements DateTimeParameter {

    @Parameter
    @Summary("Departure DateTime")
    @DisplayName("Departure DateTime")
    @Example("2025-10-25T:20:00:00")
    @Placement(tab = ADVANCED_TAB)
    @Expression(ExpressionSupport.SUPPORTED)
    @ParameterDsl(allowReferences = false)
    private LocalDateTime departureDateTime;

    public LocalDateTime getDepartureDateTime() {
        return departureDateTime;
    }

    @Override
    public String getType() {
        return GoogleConstantsUtil.DEPARTURE_DATE_TIME_TYPE;
    }
}
