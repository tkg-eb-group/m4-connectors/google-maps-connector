/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.internal.operation;

import com.google.gson.Gson;
import com.google.maps.model.GeolocationResult;
import org.mule.extension.google.maps.api.param.request.GeolocationRequest;
import org.mule.extension.google.maps.internal.connection.GoogleMapsConnection;
import org.mule.extension.google.maps.internal.error.provider.GeolocationErrorsProvider;
import org.mule.extension.google.maps.internal.service.GeolocationService;
import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.dsl.xml.ParameterDsl;
import org.mule.runtime.extension.api.annotation.error.Throws;
import org.mule.runtime.extension.api.annotation.metadata.fixed.OutputJsonType;
import org.mule.runtime.extension.api.annotation.param.Connection;
import org.mule.runtime.extension.api.annotation.param.MediaType;
import org.mule.runtime.extension.api.annotation.param.NullSafe;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class GeolocationOperations {

    @MediaType(value = "application/json")
    @Throws(GeolocationErrorsProvider.class)
    @OutputJsonType(schema = "schema/geolocation-schema.json")
    @Summary("Service that returns a location and accuracy radius based on information about cell towers and WiFi nodes that the mobile client can detect")
    public InputStream geolocationRequest(@Connection GoogleMapsConnection googleMapsConnection,
                                     @Summary("Cell towers and WiFi nodes info") @DisplayName("Request Body")
                                     @Expression(ExpressionSupport.SUPPORTED) @ParameterDsl(allowReferences = false)
                                     @Optional @NullSafe GeolocationRequest geolocationRequestBody){
        GeolocationResult result = new GeolocationService().getGeolocation(googleMapsConnection.getGeoApiContext(), geolocationRequestBody);
        return new ByteArrayInputStream(new Gson().toJson(result).getBytes(StandardCharsets.UTF_8));
    }
}
