/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.api.param;

import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.dsl.xml.ParameterDsl;
import org.mule.runtime.extension.api.annotation.param.ExclusiveOptionals;
import org.mule.runtime.extension.api.annotation.param.NullSafe;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

import java.util.List;

@ExclusiveOptionals(isOneRequired = true)
public class MapsGeneralParameter {

    @Parameter
    @Optional
    @Summary("Location Parameters")
    @DisplayName("Location Parameters")
    @Expression(ExpressionSupport.NOT_SUPPORTED)
    @ParameterDsl(allowReferences = false)
    private MapCenterZoomParameter locationParameters;

    @Parameter
    @Summary("Define one or more markers to attach to the image at specified locations")
    @DisplayName("Markers")
    @Optional
    @NullSafe
    @Expression(ExpressionSupport.NOT_SUPPORTED)
    @ParameterDsl(allowReferences = false)
    private List<MarkerParameter> markers;

    @Parameter
    @Summary("Defines a single path of two or more connected points to overlay on the image at specified locations")
    @DisplayName("Paths")
    @Optional
    @NullSafe
    @Expression(ExpressionSupport.NOT_SUPPORTED)
    @ParameterDsl(allowReferences = false)
    private List<MapPathPointsParameter> mapPaths;

    public MapCenterZoomParameter getLocationParameters() {
        return locationParameters;
    }

    public List<MarkerParameter> getMarkers() {
        return markers;
    }

    public List<MapPathPointsParameter> getMapPaths() {
        return mapPaths;
    }
}


