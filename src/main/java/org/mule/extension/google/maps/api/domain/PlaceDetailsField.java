/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.api.domain;

public enum PlaceDetailsField {

    ADDRESS_COMPONENT("address_component"),
    ADR_ADDRESS("adr_address"),
    BUSINESS_STATUS("business_status"),
    FORMATTED_ADDRESS("formatted_address"),
    FORMATTED_PHONE_NUMBER("formatted_phone_number"),
    GEOMETRY("geometry"),
    GEOMETRY_LOCATION("geometry/location"),
    GEOMETRY_LOCATION_LAT("geometry/location/lat"),
    GEOMETRY_LOCATION_LNG("geometry/location/lng"),
    GEOMETRY_VIEWPORT("geometry/viewport"),
    GEOMETRY_VIEWPORT_NORTHEAST("geometry/viewport/northeast"),
    GEOMETRY_VIEWPORT_NORTHEAST_LAT("geometry/viewport/northeast/lat"),
    GEOMETRY_VIEWPORT_NORTHEAST_LNG("geometry/viewport/northeast/lng"),
    GEOMETRY_VIEWPORT_SOUTHWEST("geometry/viewport/southwest"),
    GEOMETRY_VIEWPORT_SOUTHWEST_LAT("geometry/viewport/southwest/lat"),
    GEOMETRY_VIEWPORT_SOUTHWEST_LNG("geometry/viewport/southwest/lng"),
    ICON("icon"),
    INTERNATIONAL_PHONE_NUMBER("international_phone_number"),
    NAME("name"),
    OPENING_HOURS("opening_hours"),
    USER_RATINGS_TOTAL("user_ratings_total"),
    PHOTOS("photos"),
    PLACE_ID("place_id"),
    PLUS_CODE("plus_code"),
    PRICE_LEVEL("price_level"),
    RATING("rating"),
    REVIEW("review"),
    TYPES("types"),
    URL("url"),
    UTC_OFFSET("utc_offset"),
    VICINITY("vicinity"),
    WEBSITE("website");

    private final String field;

    PlaceDetailsField(String field) {
        this.field = field;
    }

    public String getField() {
        return field;
    }
}
