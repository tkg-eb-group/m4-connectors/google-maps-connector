/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.internal.extension;

import org.mule.extension.google.maps.api.param.*;
import org.mule.extension.google.maps.api.param.interfaces.*;
import org.mule.extension.google.maps.internal.configuration.GoogleMapsConfiguration;
import org.mule.extension.google.maps.internal.error.GoogleMapsErrorType;
import org.mule.runtime.extension.api.annotation.Extension;
import org.mule.runtime.extension.api.annotation.Configurations;
import org.mule.runtime.extension.api.annotation.SubTypeMapping;
import org.mule.runtime.extension.api.annotation.dsl.xml.Xml;
import org.mule.runtime.extension.api.annotation.error.ErrorTypes;
import org.mule.runtime.extension.api.annotation.license.RequiresEnterpriseLicense;

import static org.mule.runtime.api.meta.Category.CERTIFIED;


/**
 * This is the main class of an extension, is the entry point from which configurations, connection providers, operations
 * and sources are going to be declared.
 */
@Xml(prefix = "google-maps")
@Extension(name = "Google Maps", vendor = "Ksquare", category = CERTIFIED)
@Configurations(GoogleMapsConfiguration.class)
@ErrorTypes(GoogleMapsErrorType.class)
@RequiresEnterpriseLicense(allowEvaluationLicense = true)
@SubTypeMapping(baseType = DirectionsOriginDestinationParameter.class, subTypes = {AddressParameter.class, LatitudeLongitudeParameter.class, PlaceIdParameter.class})
@SubTypeMapping(baseType = DateTimeParameter.class, subTypes = {ArrivalDateTimeParameter.class, DepartureDateTimeParameter.class})
@SubTypeMapping(baseType = ElevationParameter.class, subTypes = {PositionParameter.class, SampledPathParameter.class})
@SubTypeMapping(baseType = CoordinateParameter.class, subTypes = {LatLngCoordinateParameter.class, PolylineCoordinateParameter.class})
@SubTypeMapping(baseType = DistanceMatrixOriginDestinationParameter.class, subTypes = {DistanceAddressParameter.class, DistanceLatitudeLongitudeParameter.class, DistancePlaceIdParameter.class, DistancePolylineParameter.class})
@SubTypeMapping(baseType = LocationBiasParameter.class, subTypes = {IPBiasParameter.class, PointParameter.class, CircularParameter.class, RectangularParameter.class})
@SubTypeMapping(baseType = MapsLocationParameter.class, subTypes = {MapsLocationLatitudeLongitudeParameter.class, MapsLocationAddressParameter.class})
@SubTypeMapping(baseType = InputTypeParameter.class, subTypes = {TextQueryParameter.class, PhoneNumberParameter.class})
public class GoogleMapsConnector {

}
