/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.api.domain;

public enum RankBy {

    PROMINENCE("prominence"), DISTANCE("distance");

    private String order;

    RankBy(String order) {
        this.order = order;
    }

    public String getOrder() {
        return order;
    }
}
