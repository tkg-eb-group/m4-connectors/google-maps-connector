/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.api.param;

import org.mule.extension.google.maps.api.param.interfaces.CoordinateParameter;
import org.mule.extension.google.maps.api.param.interfaces.DistanceMatrixOriginDestinationParameter;
import org.mule.extension.google.maps.internal.util.GoogleConstantsUtil;
import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Alias;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.dsl.xml.ParameterDsl;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Example;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

@Alias("PolylineCoordinates")
public class PolylineCoordinateParameter implements CoordinateParameter, DistanceMatrixOriginDestinationParameter {

    @Parameter
    @Summary("Set of encoded coordinates using the Encoded Polyline Algorithm")
    @DisplayName("Polyline Encoded Coordinates")
    @Example("lexeF{~wsZejrPjtye@:")
    @Expression(ExpressionSupport.SUPPORTED)
    @ParameterDsl(allowReferences = false)
    private String polylineCoordinates;

    @Override
    public String getType() {
        return GoogleConstantsUtil.REQUEST_POLYLINE_TYPE;
    }

    public String getPolylineCoordinates() {
        return polylineCoordinates;
    }
}
