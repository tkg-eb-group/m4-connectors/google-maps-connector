/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.api.param;

import org.mule.extension.google.maps.api.param.interfaces.LocationBiasParameter;
import org.mule.extension.google.maps.internal.util.GoogleConstantsUtil;
import org.mule.runtime.extension.api.annotation.Alias;

@Alias("Point")
public class PointParameter extends LatitudeLongitudeParameter implements LocationBiasParameter {

    @Override
    public String type() {
        return GoogleConstantsUtil.POINT;
    }
}
