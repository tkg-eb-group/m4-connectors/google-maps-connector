/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.internal.service;

import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.GeocodingApiRequest;
import com.google.maps.errors.ApiException;
import com.google.maps.model.*;
import org.mule.extension.google.maps.api.param.*;
import org.mule.extension.google.maps.internal.error.GoogleMapsErrorType;
import org.mule.extension.google.maps.internal.error.exception.GoogleMapsException;
import org.mule.extension.google.maps.internal.util.ExceptionUtil;
import org.mule.runtime.http.api.HttpConstants;

import java.io.IOException;
import java.util.Set;

public class GeocodingService {

    public GeocodingResult[] getGeocoding(GeoApiContext context, GeocodingParameter geocodingParameter,
                               BoundsParameter bounds, String language, String region){
        GeocodingApiRequest geocodingApiRequest = GeocodingApi.newRequest(context);
        if((geocodingParameter.getGeocodingAddress() == null || geocodingParameter.getGeocodingAddress().getAddress().isEmpty())
        && geocodingParameter.getComponents().isEmpty()){
            throw new GoogleMapsException("Address or component is required",
                    GoogleMapsErrorType.ILLEGAL_ARGUMENT_EXCEPTION, HttpConstants.HttpStatus.BAD_REQUEST);
        }
        setGeocodingParameters(geocodingParameter, bounds, geocodingApiRequest);
        setCommonParameters(language, region, geocodingApiRequest);
        try {
            return geocodingApiRequest.await();
        } catch (ApiException | IOException ex) {
            throw new ExceptionUtil().throwGoogleMapsException(ex);
        } catch (InterruptedException ex){
            Thread.currentThread().interrupt();
            throw new ExceptionUtil().throwGoogleMapsException(ex);
        }
    }

    private void setGeocodingParameters(GeocodingParameter geocodingParameter, BoundsParameter bounds, GeocodingApiRequest geocodingApiRequest) {
        if(geocodingParameter.getGeocodingAddress() != null && !geocodingParameter.getGeocodingAddress().getAddress().isEmpty()){
            geocodingApiRequest.address(geocodingParameter.getGeocodingAddress().getAddress());
        }
        if(!geocodingParameter.getComponents().isEmpty()){
            ComponentFilter[] componentFilters = geocodingParameter.getComponents().stream()
                    .map(cp -> new ComponentFilter(cp.getComponent().getFilter(), cp.getValue()))
                    .toArray(ComponentFilter[]::new);
            geocodingApiRequest.components(componentFilters);
        }
        if(bounds != null){
            geocodingApiRequest.bounds(new LatLng(bounds.getSouthwest().getLatitude(), bounds.getSouthwest().getLongitude()),
                    new LatLng(bounds.getNortheast().getLatitude(), bounds.getNortheast().getLongitude()));
        }
    }

    private void setCommonParameters(String language, String region, GeocodingApiRequest geocodingApiRequest) {
        if(language != null && !language.isEmpty()){
            geocodingApiRequest.language(language);
        }
        if(region != null && !region.isEmpty()){
            geocodingApiRequest.region(region);
        }
    }

    public GeocodingResult[] getReverseGeocoding(GeoApiContext context, LatitudeLongitudeParameter latitudeLongitudeParameter,
                                      Set<AddressTypeParameter> addressesTypes, Set<LocationTypeParameter> locationsTypes,
                                      String language){
        GeocodingApiRequest geocodingApiRequest = GeocodingApi.newRequest(context);
        geocodingApiRequest.latlng(new LatLng(latitudeLongitudeParameter.getLatitude(), latitudeLongitudeParameter.getLongitude()));
        setReverseGeocodingParameters(addressesTypes, locationsTypes, language, geocodingApiRequest);
        try {
            return geocodingApiRequest.await();
        } catch (ApiException | IOException ex) {
            throw new ExceptionUtil().throwGoogleMapsException(ex);
        } catch (InterruptedException ex){
            Thread.currentThread().interrupt();
            throw new ExceptionUtil().throwGoogleMapsException(ex);
        }
    }

    private void setReverseGeocodingParameters(Set<AddressTypeParameter> addressesTypes, Set<LocationTypeParameter> locationsTypes, String language, GeocodingApiRequest geocodingApiRequest) {
        if(language != null && !language.isEmpty()){
            geocodingApiRequest.language(language);
        }
        if(!addressesTypes.isEmpty()){
            AddressType[] addressTypes = addressesTypes.stream().map(addressTypeParameter -> Enum.valueOf(AddressType.class, addressTypeParameter.getAddressType().name())).toArray(AddressType[]::new);
            geocodingApiRequest.resultType(addressTypes);
        }
        if(!locationsTypes.isEmpty()){
            LocationType[] locationTypes = locationsTypes.stream().map(locationTypeParameter -> Enum.valueOf(LocationType.class, locationTypeParameter.getLocationType().name())).toArray(LocationType[]::new);
            geocodingApiRequest.locationType(locationTypes);
        }
    }

    public GeocodingResult[] getAddressByPlaceId(GeoApiContext context, PlaceIdParameter placeIdParameter){
        GeocodingApiRequest geocodingApiRequest = GeocodingApi.newRequest(context);
        if(placeIdParameter != null && !placeIdParameter.getPlaceId().isEmpty()){
            geocodingApiRequest.place(placeIdParameter.getPlaceId());
        }
        try {
            return geocodingApiRequest.await();
        } catch (ApiException | IOException ex) {
            throw new ExceptionUtil().throwGoogleMapsException(ex);
        } catch (InterruptedException ex){
            Thread.currentThread().interrupt();
            throw new ExceptionUtil().throwGoogleMapsException(ex);
        }
    }
}
