/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.api.param;

import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Alias;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.dsl.xml.ParameterDsl;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Example;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

public class SearchLocationParameter {

    @Parameter
    @Summary("The latitude/longitude around which to retrieve place information")
    @Alias("Location")
    @DisplayName("Location")
    @Expression(ExpressionSupport.NOT_SUPPORTED)
    @ParameterDsl(allowReferences = false)
    private LatitudeLongitudeParameter latitudeLongitude;

    @Parameter
    @Summary("Defines the distance (in meters) within which to return place results")
    @DisplayName("Radius")
    @Example("1500")
    @Expression(ExpressionSupport.SUPPORTED)
    @ParameterDsl(allowReferences = false)
    private Integer radius;

    public LatitudeLongitudeParameter getLatitudeLongitude() {
        return latitudeLongitude;
    }

    public Integer getRadius() {
        return radius;
    }
}
