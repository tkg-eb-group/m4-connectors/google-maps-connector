/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.internal.service;

import com.google.maps.DirectionsApi;
import com.google.maps.DirectionsApiRequest;
import com.google.maps.GeoApiContext;
import com.google.maps.errors.ApiException;
import com.google.maps.model.*;
import org.mule.extension.google.maps.api.domain.*;
import org.mule.extension.google.maps.api.param.*;
import org.mule.extension.google.maps.api.param.interfaces.DateTimeParameter;
import org.mule.extension.google.maps.api.param.interfaces.DirectionsOriginDestinationParameter;
import org.mule.extension.google.maps.internal.error.GoogleMapsErrorType;
import org.mule.extension.google.maps.internal.error.exception.GoogleMapsException;
import org.mule.extension.google.maps.internal.util.ExceptionUtil;
import org.mule.extension.google.maps.internal.util.GoogleConstantsUtil;
import org.mule.runtime.http.api.HttpConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.time.ZoneOffset;
import java.util.Set;

public class DirectionsService {

    private static final Logger logger = LoggerFactory.getLogger(DirectionsService.class);

    public DirectionsResult getDirections(GeoApiContext context, DirectionsOriginDestinationParameter originParameter,
                                DirectionsOriginDestinationParameter destinationParameter, TravelType travelMode,
                                Set<WaypointParameter> waypoints, boolean hasAlternatives, AvoidRestriction avoid,
                                String language, String region, UnitSystem unit, DateTimeParameter dateTime,
                                TrafficType trafficModel, Set<TransitModeParameter> transitModes,
                                TransitRoutingSetting transitRoutingPreference){
        DirectionsApiRequest directionsApiRequest = DirectionsApi.newRequest(context);
        setOriginParameter(originParameter, directionsApiRequest);
        setDestinationParameter(destinationParameter, directionsApiRequest);

        setRequestParameters(travelMode, waypoints, hasAlternatives, avoid, language, region, unit, dateTime, trafficModel, transitModes, transitRoutingPreference, directionsApiRequest);
        try {
            return directionsApiRequest.await();
        } catch (ApiException | IOException ex) {
            throw new ExceptionUtil().throwGoogleMapsException(ex);
        } catch (InterruptedException ex){
            Thread.currentThread().interrupt();
            throw new ExceptionUtil().throwGoogleMapsException(ex);
        } catch (IllegalArgumentException ex){
            logger.error(String.format("Invalid request: %s", ex.getMessage()), ex);
            throw new GoogleMapsException(ex.getMessage(), GoogleMapsErrorType.INVALID_REQUEST, HttpConstants.HttpStatus.BAD_REQUEST);
        }
    }

    private void setRequestParameters(TravelType travelMode, Set<WaypointParameter> waypoints, boolean hasAlternatives, AvoidRestriction avoid, String language, String region, UnitSystem unit, DateTimeParameter dateTime, TrafficType trafficModel, Set<TransitModeParameter> transitModes, TransitRoutingSetting transitRoutingPreference, DirectionsApiRequest directionsApiRequest) {
        if(travelMode != null && !travelMode.getMode().isEmpty()){
            directionsApiRequest.mode(Enum.valueOf(TravelMode.class, travelMode.name()));
        }
        if(!waypoints.isEmpty()){
            directionsApiRequest.waypoints(waypoints.stream()
                    .map(WaypointParameter::getWaypoint).toArray(String[]::new));
        }
        if(hasAlternatives){
            directionsApiRequest.alternatives(true);
        }
        if(avoid != null && !avoid.getValue().isEmpty()){
            directionsApiRequest.avoid(Enum.valueOf(DirectionsApi.RouteRestriction.class, avoid.name()));
        }
        setCommonParameters(language, region, unit, directionsApiRequest);
        setDateTimeParameter(dateTime, directionsApiRequest);
        setTransitParameters(trafficModel, transitModes, transitRoutingPreference, directionsApiRequest);
    }

    private void setCommonParameters(String language, String region, UnitSystem unit, DirectionsApiRequest directionsApiRequest) {
        if(language != null && !language.isEmpty()){
            directionsApiRequest.language(language);
        }
        if(unit != null && !unit.getSystem().isEmpty()){
            directionsApiRequest.units(Enum.valueOf(Unit.class, unit.name()));
        }
        if(region != null && !region.isEmpty()){
            directionsApiRequest.region(region);
        }
    }

    private void setTransitParameters(TrafficType trafficModel, Set<TransitModeParameter> transitModes, TransitRoutingSetting transitRoutingPreference, DirectionsApiRequest directionsApiRequest) {
        if(trafficModel != null && !trafficModel.getName().isEmpty()){
            directionsApiRequest.trafficModel(Enum.valueOf(TrafficModel.class, trafficModel.name()));
        }
        if(!transitModes.isEmpty()){
            TransitMode[] transitModesArray = transitModes.stream().map(transitModeParameter -> Enum.valueOf(TransitMode.class, transitModeParameter.getTransitMode().name())).toArray(TransitMode[]::new);
            directionsApiRequest.transitMode(transitModesArray);
        }
        if(transitRoutingPreference != null && !transitRoutingPreference.getValue().isEmpty()){
            directionsApiRequest.transitRoutingPreference(Enum.valueOf(TransitRoutingPreference.class, transitRoutingPreference.name()));
        }
    }

    private void setDateTimeParameter(DateTimeParameter dateTime, DirectionsApiRequest directionsApiRequest) {
        if(dateTime != null){
            if(dateTime.getType().equals(GoogleConstantsUtil.ARRIVAL_DATE_TIME_TYPE)){
                ArrivalDateTimeParameter arrivalTime = (ArrivalDateTimeParameter) dateTime;
                directionsApiRequest.arrivalTime(arrivalTime.getArrivalDateTime().toInstant(ZoneOffset.UTC));
            }
            else {
                DepartureDateTimeParameter departureTime = (DepartureDateTimeParameter) dateTime;
                directionsApiRequest.departureTime(departureTime.getDepartureDateTime().toInstant(ZoneOffset.UTC));
            }
        }
    }

    private void setOriginParameter(DirectionsOriginDestinationParameter originParameter, DirectionsApiRequest directionsApiRequest) {
        switch (originParameter.getType()){
            case GoogleConstantsUtil.ORIGIN_DESTINATION_ADDRESS_TYPE:
                AddressParameter originAddressParameter = (AddressParameter) originParameter;
                directionsApiRequest.origin(originAddressParameter.getAddress());
                break;
            case GoogleConstantsUtil.ORIGIN_DESTINATION_LATLNG:
                LatitudeLongitudeParameter originLatitudeLongitudeParameter = (LatitudeLongitudeParameter) originParameter;
                directionsApiRequest.origin(new LatLng(originLatitudeLongitudeParameter.getLatitude(), originLatitudeLongitudeParameter.getLongitude()));
                break;
            case GoogleConstantsUtil.ORIGIN_DESTINATION_PLACEID_TYPE:
                PlaceIdParameter originOriginDestinationPlaceIdParameter = (PlaceIdParameter) originParameter;
                directionsApiRequest.originPlaceId(originOriginDestinationPlaceIdParameter.getPlaceId());
                break;
            default:
                break;
        }
    }

    private void setDestinationParameter(DirectionsOriginDestinationParameter destinationParameter, DirectionsApiRequest directionsApiRequest) {
        switch (destinationParameter.getType()){
            case GoogleConstantsUtil.ORIGIN_DESTINATION_ADDRESS_TYPE:
                AddressParameter originAddressParameter = (AddressParameter) destinationParameter;
                directionsApiRequest.destination(originAddressParameter.getAddress());
                break;
            case GoogleConstantsUtil.ORIGIN_DESTINATION_LATLNG:
                LatitudeLongitudeParameter originLatitudeLongitudeParameter = (LatitudeLongitudeParameter) destinationParameter;
                directionsApiRequest.destination(new LatLng(originLatitudeLongitudeParameter.getLatitude(), originLatitudeLongitudeParameter.getLongitude()));
                break;
            case GoogleConstantsUtil.ORIGIN_DESTINATION_PLACEID_TYPE:
                PlaceIdParameter originOriginDestinationPlaceIdParameter = (PlaceIdParameter) destinationParameter;
                directionsApiRequest.destinationPlaceId(originOriginDestinationPlaceIdParameter.getPlaceId());
                break;
            default:
                break;
        }
    }
}
