/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.internal.service;

import com.google.maps.GeoApiContext;
import com.google.maps.ImageResult;
import com.google.maps.StaticMapsApi;
import com.google.maps.StaticMapsRequest;
import com.google.maps.errors.ApiException;
import com.google.maps.model.LatLng;
import com.google.maps.model.Size;
import org.mule.extension.google.maps.api.domain.ImageFormat;
import org.mule.extension.google.maps.api.domain.MapType;
import org.mule.extension.google.maps.api.param.*;
import org.mule.extension.google.maps.api.param.interfaces.MapsLocationParameter;
import org.mule.extension.google.maps.internal.error.GoogleMapsErrorType;
import org.mule.extension.google.maps.internal.error.exception.GoogleMapsException;
import org.mule.extension.google.maps.internal.util.ExceptionUtil;
import org.mule.extension.google.maps.internal.util.GoogleConstantsUtil;
import org.mule.extension.google.maps.internal.util.ImageUtil;
import org.mule.runtime.extension.api.runtime.operation.Result;
import org.mule.runtime.http.api.HttpConstants;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class StaticMapsService {

    public Result<InputStream, Void> getStaticMap(GeoApiContext context, MapSizeParameter mapSizeParameter,
                               MapsGeneralParameter mapsGeneralParameter, Integer scale, ImageFormat imageFormat,
                                                  MapType mapType, String language, String region) {
        StaticMapsRequest staticMapsRequest = StaticMapsApi.newRequest(context, new Size(mapSizeParameter.getWidth(), mapSizeParameter.getHeight()));
        if(mapsGeneralParameter.getLocationParameters() != null) {
            setMapsLocationParameters(mapsGeneralParameter, staticMapsRequest);
        } else {
            if(mapsGeneralParameter.getMarkers() != null && !mapsGeneralParameter.getMarkers().isEmpty()){
                setMarkers(mapsGeneralParameter, staticMapsRequest);
            } else {
                if(mapsGeneralParameter.getMapPaths() != null){
                    setMapPathParameters(mapsGeneralParameter, staticMapsRequest);
                } else {
                    throw new GoogleMapsException("Request must contain 'center' and 'zoom' if 'markers' or 'path' aren't present.",
                            GoogleMapsErrorType.ILLEGAL_ARGUMENT_EXCEPTION, HttpConstants.HttpStatus.BAD_REQUEST);
                }
            }
        }
        setMapParameters(scale, imageFormat, mapType, language, region, staticMapsRequest);
        try {
            ImageResult result = staticMapsRequest.await();
            return Result.<InputStream, Void>builder()
                    .output(new ByteArrayInputStream(result.imageData))
                    .mediaType(new ImageUtil().getMediaType(result.contentType))
                    .build();
        } catch (ApiException | IOException ex) {
            throw new ExceptionUtil().throwGoogleMapsException(ex);
        } catch (InterruptedException ex){
            Thread.currentThread().interrupt();
            throw new ExceptionUtil().throwGoogleMapsException(ex);
        }
    }

    private void setMarkers(MapsGeneralParameter mapsGeneralParameter, StaticMapsRequest staticMapsRequest) {
        for(MarkerParameter markerParameter : mapsGeneralParameter.getMarkers()){
            StaticMapsRequest.Markers markers = setMarkerParameters(markerParameter);
            if(markerParameter.getCustomIconParameter() != null){
                setCustomIconMarker(markerParameter, markers);
            }
            if(markerParameter.getMarkerLocations() != null) {
                for(MapsLocationParameter mapsLocationParameter : markerParameter.getMarkerLocations()){
                    setMarkerLocation(markers, mapsLocationParameter);
                }
                staticMapsRequest.markers(markers);
            }
        }
    }

    private void setCustomIconMarker(MarkerParameter markerParameter, StaticMapsRequest.Markers markers) {
        CustomIconParameter customIconParameter = markerParameter.getCustomIconParameter();
        if(customIconParameter.getScale() != null){
            markers.customIcon(customIconParameter.getCustomIconURL(),
                    Enum.valueOf(StaticMapsRequest.Markers.CustomIconAnchor.class, customIconParameter.getAnchorPoint().getAnchor()),
                    customIconParameter.getScale());
        } else {
            markers.customIcon(customIconParameter.getCustomIconURL(),
                    Enum.valueOf(StaticMapsRequest.Markers.CustomIconAnchor.class, customIconParameter.getAnchorPoint().getAnchor()));
        }
    }

    private StaticMapsRequest.Markers setMarkerParameters(MarkerParameter markerParameter) {
        StaticMapsRequest.Markers markers = new StaticMapsRequest.Markers();
        if(markerParameter.getSize() != null){
            markers.size(Enum.valueOf(StaticMapsRequest.Markers.MarkersSize.class, markerParameter.getSize().getSize()));
        }
        if(markerParameter.getColor() != null){
            markers.color(markerParameter.getColor());
        }
        if(markerParameter.getLabel() != null){
            markers.label(markerParameter.getLabel());
        }
        return markers;
    }

    private void setMapParameters(Integer scale, ImageFormat imageFormat, MapType mapType, String language, String region, StaticMapsRequest staticMapsRequest) {
        if(scale != null){
            staticMapsRequest.scale(scale);
        }
        if(imageFormat != null){
            staticMapsRequest.format(Enum.valueOf(StaticMapsRequest.ImageFormat.class, imageFormat.getFormat()));
        }
        if(mapType != null){
            staticMapsRequest.maptype(Enum.valueOf(StaticMapsRequest.StaticMapType.class, mapType.getType()));
        }
        if(language != null){
            staticMapsRequest.language(language);
        }
        if(region != null){
            staticMapsRequest.region(region);
        }
    }

    private void setMapPathParameters(MapsGeneralParameter mapsGeneralParameter, StaticMapsRequest staticMapsRequest) {
        List<MapPathPointsParameter> mapPathsParameters = mapsGeneralParameter.getMapPaths();
        for (MapPathPointsParameter mapPathsParameter : mapPathsParameters){
            StaticMapsRequest.Path path = new StaticMapsRequest.Path();
            if(mapPathsParameter.getWeight() != null){
                path.weight(mapPathsParameter.getWeight());
            }
            if(mapPathsParameter.getColor() != null){
                path.color(mapPathsParameter.getColor());
            }
            if(mapPathsParameter.getFillColor() != null){
                path.fillcolor(mapPathsParameter.getFillColor());
            }
            if(mapPathsParameter.isGeodesic()){
                path.geodesic(true);
            }
            if(mapPathsParameter.getMapPointsParameter() != null && mapPathsParameter.getMapPointsParameter().getPathPoints() != null
                    && !mapPathsParameter.getMapPointsParameter().getPathPoints().isEmpty()){
                setMapPath(staticMapsRequest, mapPathsParameter, path);
            }
        }
    }

    private void setMapPath(StaticMapsRequest staticMapsRequest, MapPathPointsParameter mapPathsParameter, StaticMapsRequest.Path path) {
        for(MapsLocationParameter mapsLocationParameter : mapPathsParameter.getMapPointsParameter().getPathPoints()){
            if(mapsLocationParameter.getType().equals(GoogleConstantsUtil.ORIGIN_DESTINATION_LATLNG)){
                MapsLocationLatitudeLongitudeParameter mapsLocationLatitudeLongitudeParameter = (MapsLocationLatitudeLongitudeParameter) mapsLocationParameter;
                path.addPoint(new LatLng(mapsLocationLatitudeLongitudeParameter.getLatitude(), mapsLocationLatitudeLongitudeParameter.getLongitude()));
                staticMapsRequest.path(path);
            } else if(mapsLocationParameter.getType().equals(GoogleConstantsUtil.ORIGIN_DESTINATION_ADDRESS_TYPE)){
                MapsLocationAddressParameter mapsLocationAddressParameter = (MapsLocationAddressParameter) mapsLocationParameter;
                path.addPoint(mapsLocationAddressParameter.getAddress());
                staticMapsRequest.path(path);
            }
        }
    }

    private void setMarkerLocation(StaticMapsRequest.Markers markers, MapsLocationParameter mapsLocationParameter) {
        if(mapsLocationParameter.getType().equals(GoogleConstantsUtil.ORIGIN_DESTINATION_LATLNG)){
            MapsLocationLatitudeLongitudeParameter location = (MapsLocationLatitudeLongitudeParameter) mapsLocationParameter;
            markers.addLocation(location.getLatitude() + "," + location.getLongitude());
        } else {
            MapsLocationAddressParameter address = (MapsLocationAddressParameter) mapsLocationParameter;
            markers.addLocation(address.getAddress());
        }
    }

    private void setMapsLocationParameters(MapsGeneralParameter mapsGeneralParameter, StaticMapsRequest staticMapsRequest) {
        MapsLocationParameter mapsLocationParameter = mapsGeneralParameter.getLocationParameters().getCenter();
        if(mapsLocationParameter.getType().equals(GoogleConstantsUtil.ORIGIN_DESTINATION_LATLNG)){
            MapsLocationLatitudeLongitudeParameter centerLocation = (MapsLocationLatitudeLongitudeParameter) mapsLocationParameter;
            staticMapsRequest.center(new LatLng(centerLocation.getLatitude(), centerLocation.getLongitude()));
        } else {
            MapsLocationAddressParameter centerAddress = (MapsLocationAddressParameter) mapsLocationParameter;
            staticMapsRequest.center(centerAddress.getAddress());
        }
        if(mapsGeneralParameter.getLocationParameters().getZoom() != null){
            staticMapsRequest.zoom(mapsGeneralParameter.getLocationParameters().getZoom());
        }
    }
}
