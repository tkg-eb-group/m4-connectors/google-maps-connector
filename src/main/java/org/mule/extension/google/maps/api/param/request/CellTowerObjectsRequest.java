/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.api.param.request;

import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.dsl.xml.ParameterDsl;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

import java.io.Serializable;

public class CellTowerObjectsRequest implements Serializable {

    @Parameter
    @Summary("Cell Id")
    @DisplayName("Cell Id")
    @Expression(ExpressionSupport.SUPPORTED)
    @ParameterDsl(allowReferences = false)
    private int cellId;

    @Parameter
    @Summary("Location Area Code")
    @DisplayName("Location Area Code")
    @Expression(ExpressionSupport.SUPPORTED)
    @ParameterDsl(allowReferences = false)
    private int locationAreaCode;

    @Parameter
    @Summary("Mobile Country Code")
    @DisplayName("Mobile Country Code")
    @Expression(ExpressionSupport.SUPPORTED)
    @ParameterDsl(allowReferences = false)
    private int mobileCountryCode;

    @Parameter
    @Summary("Mobile Network Code")
    @DisplayName("Mobile Network Code")
    @Expression(ExpressionSupport.SUPPORTED)
    @ParameterDsl(allowReferences = false)
    private int mobileNetworkCode;

    @Parameter
    @Summary("Age")
    @DisplayName("Age")
    @Optional
    @Expression(ExpressionSupport.SUPPORTED)
    @ParameterDsl(allowReferences = false)
    private int age;

    @Parameter
    @Summary("Signal Strength")
    @DisplayName("Signal Strength")
    @Optional
    @Expression(ExpressionSupport.SUPPORTED)
    @ParameterDsl(allowReferences = false)
    private int signalStrength;

    @Parameter
    @Summary("Timing Advance")
    @DisplayName("Timing Advance")
    @Optional
    @Expression(ExpressionSupport.SUPPORTED)
    @ParameterDsl(allowReferences = false)
    private int timingAdvance;

    public int getCellId() {
        return cellId;
    }

    public int getLocationAreaCode() {
        return locationAreaCode;
    }

    public int getMobileCountryCode() {
        return mobileCountryCode;
    }

    public int getMobileNetworkCode() {
        return mobileNetworkCode;
    }

    public int getAge() {
        return age;
    }

    public int getSignalStrength() {
        return signalStrength;
    }

    public int getTimingAdvance() {
        return timingAdvance;
    }
}
