/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.api.param;

import org.mule.extension.google.maps.api.domain.MarkerSize;
import org.mule.extension.google.maps.api.param.interfaces.MapsLocationParameter;
import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Alias;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.dsl.xml.ParameterDsl;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Example;
import org.mule.runtime.extension.api.annotation.param.display.Placement;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

import java.util.List;

import static org.mule.runtime.extension.api.annotation.param.display.Placement.ADVANCED_TAB;

@Alias("Marker")
public class MarkerParameter {

    @Parameter
    @Summary("Marker Size")
    @DisplayName("Size")
    @Optional
    @Placement(tab = ADVANCED_TAB)
    @Expression(ExpressionSupport.NOT_SUPPORTED)
    @ParameterDsl(allowReferences = false)
    private MarkerSize size;

    @Parameter
    @Summary("Marker Color")
    @DisplayName("Color")
    @Example("0xFFFFCCFF | red")
    @Optional
    @Placement(tab = ADVANCED_TAB)
    @Expression(ExpressionSupport.SUPPORTED)
    @ParameterDsl(allowReferences = false)
    private String color;

    @Parameter
    @Summary("Marker Label")
    @DisplayName("Label")
    @Example("VALUE")
    @Optional
    @Placement(tab = ADVANCED_TAB)
    @Expression(ExpressionSupport.SUPPORTED)
    @ParameterDsl(allowReferences = false)
    private String label;

    @Parameter
    @Summary("Custom Icon Parameter")
    @DisplayName("Custom Icon")
    @Optional
    @Placement(tab = ADVANCED_TAB)
    @Expression(ExpressionSupport.NOT_SUPPORTED)
    @ParameterDsl(allowReferences = false)
    private CustomIconParameter customIconParameter;

    @Parameter
    @Summary("Markers Locations")
    @DisplayName("Locations")
    @Expression(ExpressionSupport.NOT_SUPPORTED)
    @ParameterDsl(allowReferences = false)
    private List<MapsLocationParameter> markerLocations;

    public MarkerSize getSize() {
        return size;
    }

    public String getColor() {
        return color;
    }

    public String getLabel() {
        return label;
    }

    public CustomIconParameter getCustomIconParameter() {
        return customIconParameter;
    }

    public List<MapsLocationParameter> getMarkerLocations() {
        return markerLocations;
    }
}
