/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.internal.util;

public class GoogleConstantsUtil {

    public static final String ADDITIONAL_PARAMETERS = "Additional Parameters";
    public static final String ARRIVAL_DATE_TIME_TYPE = "Arrival Date";
    public static final String DEPARTURE_DATE_TIME_TYPE = "Departure Date";
    public static final String POSITIONAL_REQUEST_TYPE = "Positional Request";
    public static final String SAMPLED_PATH_REQUEST_TYPE = "Sampled Path Request";
    public static final String REQUEST_LAT_LNG_TYPE = "Lat Lng Positional Request";
    public static final String REQUEST_POLYLINE_TYPE = "Polyline Positional Request";
    public static final String ORIGIN_DESTINATION_ADDRESS_TYPE = "Address";
    public static final String ORIGIN_DESTINATION_LATLNG = "LatLng";
    public static final String ORIGIN_DESTINATION_PLACEID_TYPE = "Place ID";
    public static final String IP_BIAS = "ipbias";
    public static final String POINT = "point";
    public static final String CIRCULAR = "circular";
    public static final String RECTANGULAR = "rectangular";
    public static final String TEXT_QUERY = "text_query";
    public static final String PHONE_NUMBER = "phone_number";

    private GoogleConstantsUtil() {
    }
}
