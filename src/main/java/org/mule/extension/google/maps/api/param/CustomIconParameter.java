/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.api.param;

import org.mule.extension.google.maps.api.domain.CustomIconAnchor;
import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Alias;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.dsl.xml.ParameterDsl;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Example;
import org.mule.runtime.extension.api.annotation.param.display.Placement;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

import static org.mule.runtime.extension.api.annotation.param.display.Placement.ADVANCED_TAB;

@Alias("Custom Icon")
public class CustomIconParameter {

    @Parameter
    @Summary("Marker Custom Icon URL")
    @DisplayName("Custom Icon URL")
    @Example("https://developers.google.com/maps/documentation/javascript/examples/full/images/info-i_maps.png")
    @Placement(tab = ADVANCED_TAB)
    @Expression(ExpressionSupport.SUPPORTED)
    @ParameterDsl(allowReferences = false)
    private String customIconURL;

    @Parameter
    @Summary("Marker Anchor Point")
    @DisplayName("Anchor Point")
    @Placement(tab = ADVANCED_TAB)
    @Expression(ExpressionSupport.NOT_SUPPORTED)
    @ParameterDsl(allowReferences = false)
    private CustomIconAnchor anchorPoint;

    @Parameter
    @Summary("Marker Scale")
    @DisplayName("Scale")
    @Example("1")
    @Optional
    @Placement(tab = ADVANCED_TAB)
    @Expression(ExpressionSupport.SUPPORTED)
    @ParameterDsl(allowReferences = false)
    private Integer scale;

    public String getCustomIconURL() {
        return customIconURL;
    }

    public CustomIconAnchor getAnchorPoint() {
        return anchorPoint;
    }

    public Integer getScale() {
        return scale;
    }
}
