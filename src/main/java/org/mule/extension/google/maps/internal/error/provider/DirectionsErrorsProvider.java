/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.internal.error.provider;

import org.mule.extension.google.maps.internal.error.GoogleMapsErrorType;
import org.mule.runtime.extension.api.annotation.error.ErrorTypeProvider;
import org.mule.runtime.extension.api.error.ErrorTypeDefinition;

import java.util.HashSet;
import java.util.Set;

public class DirectionsErrorsProvider implements ErrorTypeProvider {

    @Override
    public Set<ErrorTypeDefinition> getErrorTypes() {
        Set<ErrorTypeDefinition<GoogleMapsErrorType>> errors = new HashSet<>();
        errors.add(GoogleMapsErrorType.NOT_FOUND);
        errors.add(GoogleMapsErrorType.ZERO_RESULTS);
        errors.add(GoogleMapsErrorType.MAX_WAYPOINTS_EXCEEDED);
        errors.add(GoogleMapsErrorType.MAX_ROUTE_LENGTH_EXCEEDED);
        errors.add(GoogleMapsErrorType.INVALID_REQUEST);
        errors.add(GoogleMapsErrorType.OVER_DAILY_LIMIT);
        errors.add(GoogleMapsErrorType.OVER_QUERY_LIMIT);
        errors.add(GoogleMapsErrorType.REQUEST_DENIED);
        errors.add(GoogleMapsErrorType.UNKNOWN_ERROR);
        errors.add(GoogleMapsErrorType.INTERNAL_SERVER_ERROR);
        errors.add(GoogleMapsErrorType.ILLEGAL_ARGUMENT_EXCEPTION);
        return new HashSet<>(errors);
    }
}
