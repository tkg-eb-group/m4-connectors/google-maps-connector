/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.internal.util;

import org.mule.runtime.api.metadata.MediaType;

public class ImageUtil {

    private static final String IMAGE = "image";

    public MediaType getMediaType(String mediaType){
        switch (mediaType){
            case "image/png": return MediaType.create(IMAGE, "png");
            case "image/jpeg": return MediaType.create(IMAGE, "jpeg");
            case "image/gif": return MediaType.create(IMAGE, "gif");
            default: throw new IllegalArgumentException("Illegal media type");
        }
    }
}
