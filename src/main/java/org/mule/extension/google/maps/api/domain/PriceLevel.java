/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.api.domain;

public enum PriceLevel {

    FREE("0"),
    INEXPENSIVE("1"),
    MODERATE("2"),
    EXPENSIVE("3"),
    VERY_EXPENSIVE("4");

    private final String level;

    PriceLevel(String level) {
        this.level = level;
    }

    public String getLevel() {
        return level;
    }
}
