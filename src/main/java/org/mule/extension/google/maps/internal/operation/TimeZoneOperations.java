/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.internal.operation;

import com.google.gson.Gson;
import org.mule.extension.google.maps.api.param.LatitudeLongitudeParameter;
import org.mule.extension.google.maps.internal.connection.GoogleMapsConnection;
import org.mule.extension.google.maps.internal.error.provider.TimezoneErrorsProvider;
import org.mule.extension.google.maps.internal.service.TimeZoneService;
import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.dsl.xml.ParameterDsl;
import org.mule.runtime.extension.api.annotation.error.Throws;
import org.mule.runtime.extension.api.annotation.metadata.fixed.OutputJsonType;
import org.mule.runtime.extension.api.annotation.param.Connection;
import org.mule.runtime.extension.api.annotation.param.MediaType;
import org.mule.runtime.extension.api.annotation.param.ParameterGroup;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.TimeZone;

public class TimeZoneOperations {

    @MediaType(value = "application/json")
    @Throws(TimezoneErrorsProvider.class)
    @OutputJsonType(schema = "schema/timezone-schema.json")
    @Summary("Service that provides a simple interface to request the time zone for locations on the surface of the earth, as well as the time offset from UTC for each of those locations")
    public InputStream timezoneRequest(@Connection GoogleMapsConnection googleMapsConnection,
                                       @Summary("Location to look up") @ParameterGroup(name = "Location")
                                       @Expression(ExpressionSupport.NOT_SUPPORTED)
                                       @ParameterDsl(allowReferences = false) LatitudeLongitudeParameter locationTimeZone) {
        TimeZone result = new TimeZoneService().getTimeZone(googleMapsConnection.getGeoApiContext(), locationTimeZone);
        return new ByteArrayInputStream(new Gson().toJson(result).getBytes(StandardCharsets.UTF_8));
    }
}
