/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.internal.service;

import com.google.maps.GeoApiContext;
import com.google.maps.TimeZoneApi;
import com.google.maps.errors.ApiException;
import com.google.maps.model.LatLng;
import org.mule.extension.google.maps.api.param.LatitudeLongitudeParameter;
import org.mule.extension.google.maps.internal.util.ExceptionUtil;

import java.io.IOException;
import java.util.TimeZone;

public class TimeZoneService {

    public TimeZone getTimeZone(GeoApiContext context, LatitudeLongitudeParameter locationTimeZone){
        try {
            return TimeZoneApi.getTimeZone(context, new LatLng(locationTimeZone.getLatitude(), locationTimeZone.getLongitude())).await();
        } catch (ApiException | IOException ex) {
            throw new ExceptionUtil().throwGoogleMapsException(ex);
        } catch (InterruptedException ex){
            Thread.currentThread().interrupt();
            throw new ExceptionUtil().throwGoogleMapsException(ex);
        }
    }
}
