/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.internal.service;

import com.google.maps.*;
import com.google.maps.errors.ApiException;
import com.google.maps.model.*;
import org.mule.extension.google.maps.api.param.*;
import org.mule.extension.google.maps.api.param.interfaces.InputTypeParameter;
import org.mule.extension.google.maps.api.param.interfaces.LocationBiasParameter;
import org.mule.extension.google.maps.internal.error.GoogleMapsErrorType;
import org.mule.extension.google.maps.internal.error.exception.GoogleMapsException;
import org.mule.extension.google.maps.internal.util.ExceptionUtil;
import org.mule.extension.google.maps.internal.util.GoogleConstantsUtil;
import org.mule.extension.google.maps.internal.util.ImageUtil;
import org.mule.runtime.extension.api.runtime.operation.Result;
import org.mule.runtime.http.api.HttpConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Set;
import java.util.UUID;

public class PlacesService {

    private static final Logger logger = LoggerFactory.getLogger(PlacesService.class);

    public FindPlaceFromText findPlace(GeoApiContext context, InputTypeParameter inputType,
                                       String language, Set<SearchPlaceFieldsParameter> placesFields,
                                       LocationBiasParameter locationBias){
        FindPlaceFromTextRequest.InputType type;
        String input;
        if(inputType.getType().equals(GoogleConstantsUtil.TEXT_QUERY)){
            type = FindPlaceFromTextRequest.InputType.TEXT_QUERY;
            TextQueryParameter textQueryParameter = (TextQueryParameter) inputType;
            input = textQueryParameter.getQuery();
        } else {
            if(inputType.getType().equals(GoogleConstantsUtil.PHONE_NUMBER)){
                type = FindPlaceFromTextRequest.InputType.PHONE_NUMBER;
                PhoneNumberParameter phoneNumberParameter = (PhoneNumberParameter) inputType;
                input = phoneNumberParameter.getPhoneNumber();
            } else {
                throw new GoogleMapsException("Invalid input type value",
                        GoogleMapsErrorType.ILLEGAL_ARGUMENT_EXCEPTION, HttpConstants.HttpStatus.BAD_REQUEST);
            }
        }
        FindPlaceFromTextRequest findPlaceFromTextRequest = PlacesApi.findPlaceFromText(context, input, type);
        setFindPlaceParameters(language, placesFields, findPlaceFromTextRequest);
        if(locationBias != null){
            setLocationBias(locationBias, findPlaceFromTextRequest);
        }
        try {
            return findPlaceFromTextRequest.await();
        } catch (ApiException | IOException ex) {
            throw new ExceptionUtil().throwGoogleMapsException(ex);
        } catch (InterruptedException ex){
            Thread.currentThread().interrupt();
            throw new ExceptionUtil().throwGoogleMapsException(ex);
        }
    }

    private void setLocationBias(LocationBiasParameter locationBias, FindPlaceFromTextRequest findPlaceFromTextRequest) {
        switch (locationBias.type()){
            case GoogleConstantsUtil.IP_BIAS:
                findPlaceFromTextRequest.locationBias(new FindPlaceFromTextRequest.LocationBiasIP());
                break;
            case GoogleConstantsUtil.POINT:
                PointParameter pointParameter = (PointParameter) locationBias;
                findPlaceFromTextRequest.locationBias(new FindPlaceFromTextRequest.LocationBiasPoint
                        (new LatLng(pointParameter.getLatitude(), pointParameter.getLongitude())));
                break;
            case GoogleConstantsUtil.CIRCULAR:
                CircularParameter circularParameter = (CircularParameter) locationBias;
                findPlaceFromTextRequest.locationBias(new FindPlaceFromTextRequest.LocationBiasCircular(
                        new LatLng(circularParameter.getLatitude(), circularParameter.getLongitude()), circularParameter.getRadius()
                ));
                break;
            case GoogleConstantsUtil.RECTANGULAR:
                setRectangularLocationBias((RectangularParameter) locationBias, findPlaceFromTextRequest);
                break;
            default:
                break;
        }
    }

    private void setFindPlaceParameters(String language, Set<SearchPlaceFieldsParameter> placesFields, FindPlaceFromTextRequest findPlaceFromTextRequest) {
        if(language != null && !language.isEmpty()){
            findPlaceFromTextRequest.language(language);
        }
        if(!placesFields.isEmpty()){
            FindPlaceFromTextRequest.FieldMask[] fields = placesFields.stream()
                    .map(fieldsParameter -> Enum.valueOf(FindPlaceFromTextRequest.FieldMask.class, fieldsParameter.getSearchPlaceField().name()))
                    .toArray(FindPlaceFromTextRequest.FieldMask[]::new);
            findPlaceFromTextRequest.fields(fields);
        }
    }

    private void setRectangularLocationBias(RectangularParameter locationBias, FindPlaceFromTextRequest findPlaceFromTextRequest) {
        RectangularParameter rectangularParameter = locationBias;
        findPlaceFromTextRequest.locationBias(new FindPlaceFromTextRequest.LocationBiasRectangular(
                new LatLng(rectangularParameter.getSouthwestLatitudeLongitudeParameter().getLatitude(),
                        rectangularParameter.getSouthwestLatitudeLongitudeParameter().getLongitude()),
                new LatLng(rectangularParameter.getNorthEastLatLngParameter().getLatitude(),
                        rectangularParameter.getNorthEastLatLngParameter().getLatitude())
        ));
    }

    public PlacesSearchResponse nearbySearch(GeoApiContext context, SearchLocationParameter latitudeLongitudeParameter,
                                             String keyword, String language, org.mule.extension.google.maps.api.domain.PriceLevel minPrice,
                                             org.mule.extension.google.maps.api.domain.PriceLevel maxPrice,
                                             String indexedName, boolean openNow,
                                             org.mule.extension.google.maps.api.domain.RankBy rankBy,
                                             org.mule.extension.google.maps.api.domain.PlaceType placeType, String pageToken){
        NearbySearchRequest nearbySearchRequest;
        nearbySearchRequest = PlacesApi.nearbySearchQuery(context,
                new LatLng(latitudeLongitudeParameter.getLatitudeLongitude().getLatitude(),
                        latitudeLongitudeParameter.getLatitudeLongitude().getLongitude()));
        setNearbySearchParameters(latitudeLongitudeParameter, keyword, language, minPrice, maxPrice, indexedName, openNow, rankBy, placeType, pageToken, nearbySearchRequest);
        try {
            return nearbySearchRequest.await();
        } catch (ApiException | IOException ex) {
            throw new ExceptionUtil().throwGoogleMapsException(ex);
        } catch (InterruptedException ex){
            Thread.currentThread().interrupt();
            throw new ExceptionUtil().throwGoogleMapsException(ex);
        }
    }

    private void setNearbySearchParameters(SearchLocationParameter latitudeLongitudeParameter, String keyword, String language, org.mule.extension.google.maps.api.domain.PriceLevel minPrice, org.mule.extension.google.maps.api.domain.PriceLevel maxPrice, String indexedName, boolean openNow, org.mule.extension.google.maps.api.domain.RankBy rankBy, org.mule.extension.google.maps.api.domain.PlaceType placeType, String pageToken, NearbySearchRequest nearbySearchRequest) {
        if(latitudeLongitudeParameter.getRadius() != null){
            nearbySearchRequest.radius(latitudeLongitudeParameter.getRadius());
        }
        if(keyword != null && !keyword.isEmpty()){
            nearbySearchRequest.keyword(keyword);
        }
        setCommonParameters(language, minPrice, maxPrice, nearbySearchRequest);
        if(indexedName != null && !indexedName.isEmpty()){
            nearbySearchRequest.name(indexedName);
        }
        if(openNow){
            nearbySearchRequest.openNow(true);
        }
        if(rankBy != null){
            nearbySearchRequest.rankby(Enum.valueOf(RankBy.class, rankBy.name()));
        }
        if(placeType != null){
            nearbySearchRequest.type(Enum.valueOf(PlaceType.class, placeType.name()));
        }
        if(pageToken != null && !pageToken.isEmpty()){
            nearbySearchRequest.pageToken(pageToken);
        }
    }

    private void setCommonParameters(String language, org.mule.extension.google.maps.api.domain.PriceLevel minPrice, org.mule.extension.google.maps.api.domain.PriceLevel maxPrice, NearbySearchRequest nearbySearchRequest) {
        if(language != null && !language.isEmpty()){
            nearbySearchRequest.language(language);
        }
        if(minPrice != null){
            nearbySearchRequest.minPrice(Enum.valueOf(PriceLevel.class, minPrice.name()));
        }
        if(maxPrice != null){
            nearbySearchRequest.maxPrice(Enum.valueOf(PriceLevel.class, maxPrice.name()));
        }
    }

    public PlacesSearchResponse textSearch(GeoApiContext context, String query,
                                           TextSearchLocationParameter textSearchLocation, String region,
                                           String language, org.mule.extension.google.maps.api.domain.PriceLevel minPrice,
                                           org.mule.extension.google.maps.api.domain.PriceLevel maxPrice,
                                           boolean openNow, String pageToken,
                                           org.mule.extension.google.maps.api.domain.PlaceType placeType){
        TextSearchRequest textSearchRequest = PlacesApi.textSearchQuery(context, query);
        setTextSearchParameters(textSearchLocation, region, language, minPrice, maxPrice, openNow, pageToken, placeType, textSearchRequest);
        try {
            return textSearchRequest.await();
        } catch (ApiException | IOException ex) {
            throw new ExceptionUtil().throwGoogleMapsException(ex);
        } catch (InterruptedException ex){
            Thread.currentThread().interrupt();
            throw new ExceptionUtil().throwGoogleMapsException(ex);
        }
    }

    private void setTextSearchParameters(TextSearchLocationParameter textSearchLocation, String region, String language, org.mule.extension.google.maps.api.domain.PriceLevel minPrice, org.mule.extension.google.maps.api.domain.PriceLevel maxPrice, boolean openNow, String pageToken, org.mule.extension.google.maps.api.domain.PlaceType placeType, TextSearchRequest textSearchRequest) {
        if(textSearchLocation != null && textSearchLocation.getTextLocation() != null){
            TextSearchLocationParameter textSearchLocationParameter = textSearchLocation;
            textSearchRequest.location(new LatLng(textSearchLocationParameter.getTextLocation().getLatitude(),
                    textSearchLocationParameter.getTextLocation().getLongitude()));
            if(textSearchLocationParameter.getRadius() != null){
                textSearchRequest.radius(textSearchLocationParameter.getRadius());
            }
        }
        setTextSearchCommonParameters(region, language, minPrice, maxPrice, textSearchRequest);
        if(openNow){
            textSearchRequest.openNow(true);
        }
        if(pageToken != null && !pageToken.isEmpty()){
            textSearchRequest.pageToken(pageToken);
        }
        if(placeType != null){
            textSearchRequest.type(Enum.valueOf(PlaceType.class, placeType.name()));
        }
    }

    private void setTextSearchCommonParameters(String region, String language, org.mule.extension.google.maps.api.domain.PriceLevel minPrice, org.mule.extension.google.maps.api.domain.PriceLevel maxPrice, TextSearchRequest textSearchRequest) {
        if(region != null && !region.isEmpty()){
            textSearchRequest.region(region);
        }
        if(language != null && !language.isEmpty()){
            textSearchRequest.language(language);
        }
        if(minPrice != null){
            textSearchRequest.minPrice(Enum.valueOf(PriceLevel.class, minPrice.name()));
        }
        if(maxPrice != null){
            textSearchRequest.maxPrice(Enum.valueOf(PriceLevel.class, maxPrice.name()));
        }
    }

    public PlaceDetails placeDetails(GeoApiContext context, String placeId,
                                     String language, String region, String sessionToken,
                                     Set<PlaceDetailsFieldsParameter> placesDetailsFields){
        PlaceDetailsRequest placeDetailsRequest = PlacesApi.placeDetails(context, placeId);
        setPlaceDetailsParameters(language, region, placesDetailsFields, placeDetailsRequest);
        if(sessionToken != null && !sessionToken.isEmpty()){
            try{
                UUID token = UUID.fromString(sessionToken);
                placeDetailsRequest.sessionToken(new PlaceAutocompleteRequest.SessionToken(token));
            } catch (IllegalArgumentException ex) {
                logger.error("Invalid request: Invalid session token value", ex);
                throw new GoogleMapsException("Invalid session token value",
                        GoogleMapsErrorType.ILLEGAL_ARGUMENT_EXCEPTION, HttpConstants.HttpStatus.BAD_REQUEST);
            }
        }
        try {
            return placeDetailsRequest.await();
        } catch (ApiException | IOException ex) {
            throw new ExceptionUtil().throwGoogleMapsException(ex);
        } catch (InterruptedException ex){
            Thread.currentThread().interrupt();
            throw new ExceptionUtil().throwGoogleMapsException(ex);
        }
    }

    private void setPlaceDetailsParameters(String language, String region, Set<PlaceDetailsFieldsParameter> placesDetailsFields, PlaceDetailsRequest placeDetailsRequest) {
        if(region != null && !region.isEmpty()){
            placeDetailsRequest.region(region);
        }
        if(language != null && !language.isEmpty()){
            placeDetailsRequest.language(language);
        }
        if(!placesDetailsFields.isEmpty()){
            PlaceDetailsRequest.FieldMask[] fields = placesDetailsFields.stream()
                    .map(fieldsParameter -> Enum.valueOf(PlaceDetailsRequest.FieldMask.class, fieldsParameter.getPlaceDetailsField().name()))
                    .toArray(PlaceDetailsRequest.FieldMask[]::new);
            placeDetailsRequest.fields(fields);
        }
    }

    public Result<InputStream, Void> photosRequest(GeoApiContext context,
                                                   PhotoPropertiesParameter photoPropertiesParameter){
        PhotoRequest photoRequest = PlacesApi.photo(context, photoPropertiesParameter.getPhotoReference());
        if(photoPropertiesParameter.getMaxHeight() != null){
            photoRequest.maxHeight(photoPropertiesParameter.getMaxHeight());
        } else {
            if(photoPropertiesParameter.getMaxWidth() != null){
                photoRequest.maxWidth(photoPropertiesParameter.getMaxWidth());
            }
        }
        try {
            ImageResult result = photoRequest.await();
            return Result.<InputStream, Void>builder()
                    .output(new ByteArrayInputStream(result.imageData))
                    .mediaType(new ImageUtil().getMediaType(result.contentType))
                    .build();
        } catch (ApiException | IOException ex) {
            logger.error(String.format("Invalid photo request: %s", ex.getMessage()), ex);
            throw new GoogleMapsException(ex.getMessage(), GoogleMapsErrorType.INVALID_REQUEST, HttpConstants.HttpStatus.BAD_REQUEST);
        } catch (InterruptedException ex){
            Thread.currentThread().interrupt();
            throw new ExceptionUtil().throwGoogleMapsException(ex);
        }
    }

    public AutocompletePrediction[] placeAutocomplete(GeoApiContext context,
                                                      String input, String sessionToken, Integer offset,
                                                      LatitudeLongitudeParameter origin, SearchLocationParameter location,
                                                      boolean isStrictBounds, String language,
                                                      org.mule.extension.google.maps.api.domain.PlaceAutocompleteType placeType,
                                                      List<ComponentsParameter> components){
        PlaceAutocompleteRequest placeAutocompleteRequest = PlacesApi.placeAutocomplete(context, input, null);
        setPlaceAutocompleteParameters(sessionToken, offset, origin, location, language, placeType, placeAutocompleteRequest, components, isStrictBounds);
        try {
            return placeAutocompleteRequest.await();
        } catch (ApiException | IOException ex) {
            throw new ExceptionUtil().throwGoogleMapsException(ex);
        } catch (InterruptedException ex){
            Thread.currentThread().interrupt();
            throw new ExceptionUtil().throwGoogleMapsException(ex);
        }
    }

    private void setPlaceAutocompleteParameters(String sessionToken, Integer offset, LatitudeLongitudeParameter origin, SearchLocationParameter location, String language, org.mule.extension.google.maps.api.domain.PlaceAutocompleteType placeType, PlaceAutocompleteRequest placeAutocompleteRequest,
                                                List<ComponentsParameter> components, boolean isStrictBounds) {
        if(sessionToken != null && !sessionToken.isEmpty()){
            try{
                UUID token = UUID.fromString(sessionToken);
                placeAutocompleteRequest.sessionToken(new PlaceAutocompleteRequest.SessionToken(token));
            } catch (IllegalArgumentException ex){
                logger.error("Invalid request: Invalid session token value", ex);
                throw new GoogleMapsException("Invalid session token value",
                        GoogleMapsErrorType.ILLEGAL_ARGUMENT_EXCEPTION, HttpConstants.HttpStatus.BAD_REQUEST);
            }
        }
        if(offset != null){
            placeAutocompleteRequest.offset(offset);
        }
        setBasicPlaceAutocompleteParameters(origin, location, language, placeType, placeAutocompleteRequest);
        if(!components.isEmpty()){
            com.google.maps.model.ComponentFilter[] componentFilters = components.stream()
                    .map(cp -> new com.google.maps.model.ComponentFilter(cp.getComponent().getFilter(), cp.getValue()))
                    .toArray(com.google.maps.model.ComponentFilter[]::new);
            placeAutocompleteRequest.components(componentFilters);
        }
        if(isStrictBounds){
            placeAutocompleteRequest.strictBounds(true);
        }
    }

    private void setBasicPlaceAutocompleteParameters(LatitudeLongitudeParameter origin, SearchLocationParameter location, String language, org.mule.extension.google.maps.api.domain.PlaceAutocompleteType placeType, PlaceAutocompleteRequest placeAutocompleteRequest) {
        if(origin != null){
            placeAutocompleteRequest.origin(new LatLng(origin.getLatitude(), origin.getLongitude()));
        }
        if(location != null){
            placeAutocompleteRequest.location(new LatLng(location.getLatitudeLongitude().getLatitude(), location.getLatitudeLongitude().getLongitude()));
            if(location.getRadius() != null){
                placeAutocompleteRequest.radius(location.getRadius());
            }
        }
        if(language != null && !language.isEmpty()){
            placeAutocompleteRequest.language(language);
        }
        if(placeType != null){
            placeAutocompleteRequest.types(Enum.valueOf(PlaceAutocompleteType.class, placeType.name()));
        }
    }

    public AutocompletePrediction[] queryAutocomplete(GeoApiContext context,
                                                      String input, Integer offset,
                                                      SearchLocationParameter location, String language){
        QueryAutocompleteRequest queryAutocompleteRequest = PlacesApi.queryAutocomplete(context, input);
        setQueryAutocompleteParameters(offset, location, language, queryAutocompleteRequest);
        try {
            return queryAutocompleteRequest.await();
        } catch (ApiException | IOException ex) {
            throw new ExceptionUtil().throwGoogleMapsException(ex);
        } catch (InterruptedException ex){
            Thread.currentThread().interrupt();
            throw new ExceptionUtil().throwGoogleMapsException(ex);
        }
    }

    private void setQueryAutocompleteParameters(Integer offset, SearchLocationParameter location, String language, QueryAutocompleteRequest queryAutocompleteRequest) {
        if(offset != null){
            queryAutocompleteRequest.offset(offset);
        }
        if(location != null){
            queryAutocompleteRequest.location(new LatLng(location.getLatitudeLongitude().getLatitude(), location.getLatitudeLongitude().getLongitude()));
            if(location.getRadius() != null){
                queryAutocompleteRequest.radius(location.getRadius());
            }
        }
        if(language != null && !language.isEmpty()){
            queryAutocompleteRequest.language(language);
        }
    }
}
