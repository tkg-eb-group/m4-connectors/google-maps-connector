/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.internal.operation;

import com.google.gson.Gson;
import com.google.maps.model.ElevationResult;
import org.mule.extension.google.maps.internal.error.provider.ElevationErrorsProvider;
import org.mule.extension.google.maps.api.param.interfaces.ElevationParameter;
import org.mule.extension.google.maps.internal.connection.GoogleMapsConnection;
import org.mule.extension.google.maps.internal.service.ElevationService;
import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.dsl.xml.ParameterDsl;
import org.mule.runtime.extension.api.annotation.error.Throws;
import org.mule.runtime.extension.api.annotation.metadata.fixed.OutputJsonType;
import org.mule.runtime.extension.api.annotation.param.Connection;
import org.mule.runtime.extension.api.annotation.param.MediaType;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

public class ElevationOperations {

    @MediaType(value = "application/json")
    @Throws(ElevationErrorsProvider.class)
    @OutputJsonType(schema = "schema/elevation-schema.json")
    @Summary("Service that provides a simple interface to query locations on the earth for elevation data")
    public InputStream elevationRequest(@Connection GoogleMapsConnection googleMapsConnection,
                                    @Summary("Position or path to query locations on the earth for elevation data") @DisplayName("Elevation")
                                    @Expression(ExpressionSupport.NOT_SUPPORTED) @ParameterDsl(allowReferences = false) ElevationParameter elevationParameter){
        ElevationResult[] result = new ElevationService().getElevation(googleMapsConnection.getGeoApiContext(), elevationParameter);
        return new ByteArrayInputStream(new Gson().toJson(result).getBytes(StandardCharsets.UTF_8));
    }
}
