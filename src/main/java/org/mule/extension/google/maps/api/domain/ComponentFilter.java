/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.api.domain;

public enum ComponentFilter {

    POSTAL_CODE("postal_code"), COUNTRY("country"), ROUTE("route"),
    LOCALITY("locality"), ADMINISTRATIVE_AREA("administrative_area");

    private final String filter;

    ComponentFilter(String filter) {
        this.filter = filter;
    }

    public String getFilter() {
        return filter;
    }
}
