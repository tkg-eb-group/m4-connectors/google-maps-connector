/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.api.domain;

public enum SearchPlaceField {

    BUSINESS_STATUS("business_status"),
    FORMATTED_ADDRESS("formatted_address"),
    GEOMETRY("geometry"),
    ICON("icon"),
    ID("id"),
    NAME("name"),
    OPENING_HOURS("opening_hours"),
    PHOTOS("photos"),
    PLACE_ID("place_id"),
    PRICE_LEVEL("price_level"),
    RATING("rating"),
    TYPES("types");

    private final String field;

    SearchPlaceField(String field) {
        this.field = field;
    }

    public String getField() {
        return field;
    }
}
