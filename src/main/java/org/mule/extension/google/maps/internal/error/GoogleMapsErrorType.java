/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */

package org.mule.extension.google.maps.internal.error;

import static java.util.Optional.ofNullable;
import static org.mule.runtime.extension.api.error.MuleErrors.CONNECTIVITY;

import java.util.Optional;

import org.mule.runtime.extension.api.error.ErrorTypeDefinition;

public enum GoogleMapsErrorType implements ErrorTypeDefinition<GoogleMapsErrorType> {

    EXECUTION,
    VALIDATION,
    CONNECTION_EXCEPTION(CONNECTIVITY),
    INVALID_REQUEST(VALIDATION),
    MAX_ELEMENTS_EXCEEDED(VALIDATION),
    MAX_ROUTE_LENGTH_EXCEEDED(VALIDATION),
    MAX_WAYPOINTS_EXCEEDED(VALIDATION),
    NOT_FOUND(EXECUTION),
    OVER_DAILY_LIMIT(EXECUTION),
    OVER_QUERY_LIMIT(EXECUTION),
    REQUEST_DENIED(EXECUTION),
    UNKNOWN_ERROR(EXECUTION),
    ZERO_RESULTS(EXECUTION),
    ACCESS_NOT_CONFIGURED(EXECUTION),
    ILLEGAL_ARGUMENT_EXCEPTION(VALIDATION),
    INTERNAL_SERVER_ERROR(EXECUTION);

    private ErrorTypeDefinition<?> parent;

    GoogleMapsErrorType() {

    }

    GoogleMapsErrorType(ErrorTypeDefinition<?> parent) {
        this.parent = parent;
    }

    @Override
    public Optional<ErrorTypeDefinition<? extends Enum<?>>> getParent() {
        return ofNullable(parent);
    }
}
