/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.internal.connection;

import com.google.maps.GeoApiContext;

public final class GoogleMapsConnection {

  private GeoApiContext geoApiContext;
  private final String apiKey;

  public GoogleMapsConnection(String apiKey) {
    this.apiKey = apiKey;
  }

  public GeoApiContext getGeoApiContext() {
    return geoApiContext;
  }

  public String getApiKey() {
    return apiKey;
  }

  public GeoApiContext connect() {
     geoApiContext = new GeoApiContext.Builder().apiKey(getApiKey()).build();
     return geoApiContext;
  }

  public void invalidate() {
    geoApiContext.shutdown();
  }
}
