/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.api.domain;

public enum RadioType {

    LTE("lte"),
    GSM("gsm"),
    CDMA("cdma"),
    WCDMA("wcdma");

    private final String type;

    RadioType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
