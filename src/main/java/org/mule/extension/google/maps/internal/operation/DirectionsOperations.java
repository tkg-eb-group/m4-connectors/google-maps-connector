/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.internal.operation;

import com.google.gson.Gson;
import com.google.maps.model.DirectionsResult;
import org.mule.extension.google.maps.api.domain.*;
import org.mule.extension.google.maps.api.domain.provider.LanguageValueProvider;
import org.mule.extension.google.maps.api.domain.provider.RegionValueProvider;
import org.mule.extension.google.maps.api.param.TransitModeParameter;
import org.mule.extension.google.maps.api.param.WaypointParameter;
import org.mule.extension.google.maps.api.param.interfaces.DateTimeParameter;
import org.mule.extension.google.maps.api.param.interfaces.DirectionsOriginDestinationParameter;
import org.mule.extension.google.maps.internal.connection.GoogleMapsConnection;
import org.mule.extension.google.maps.internal.error.provider.DirectionsErrorsProvider;
import org.mule.extension.google.maps.internal.service.DirectionsService;
import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.dsl.xml.ParameterDsl;
import org.mule.runtime.extension.api.annotation.error.Throws;
import org.mule.runtime.extension.api.annotation.metadata.fixed.OutputJsonType;
import org.mule.runtime.extension.api.annotation.param.Connection;
import org.mule.runtime.extension.api.annotation.param.MediaType;
import org.mule.runtime.extension.api.annotation.param.NullSafe;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Placement;
import org.mule.runtime.extension.api.annotation.param.display.Summary;
import org.mule.runtime.extension.api.annotation.values.OfValues;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Set;

import static org.mule.runtime.extension.api.annotation.param.display.Placement.ADVANCED_TAB;

public class DirectionsOperations {

    @MediaType(value = "application/json")
    @Throws(DirectionsErrorsProvider.class)
    @OutputJsonType(schema = "schema/directions-schema.json")
    @Summary("Service that calculates directions between locations")
    public InputStream directionsRequest(@Connection GoogleMapsConnection googleMapsConnection,
                                         @Summary("The address, textual latitude/longitude value, or place ID from which you wish to calculate directions") @DisplayName("Origin") @Expression(ExpressionSupport.NOT_SUPPORTED) @ParameterDsl(allowReferences = false) DirectionsOriginDestinationParameter origin,
                                         @Summary("The address, textual latitude/longitude value, or place ID to which you wish to calculate directions") @DisplayName("Destination") @Expression(ExpressionSupport.NOT_SUPPORTED) @ParameterDsl(allowReferences = false) DirectionsOriginDestinationParameter destination,
                                         @Summary("Specifies the mode of transport to use when calculating directions")
                                         @DisplayName("Travel Mode")
                                         @Optional
                                         @Placement(tab = ADVANCED_TAB)
                                         @Expression(ExpressionSupport.NOT_SUPPORTED)
                                         @ParameterDsl(allowReferences = false) TravelType travelMode,
                                         @Summary("Intermediate locations to include in the route")
                                         @DisplayName("Waypoints")
                                         @NullSafe
                                         @Optional
                                         @Placement(tab = ADVANCED_TAB)
                                         @Expression(ExpressionSupport.NOT_SUPPORTED)
                                         @ParameterDsl(allowReferences = false) Set<WaypointParameter> waypoints,

                                         @Summary("Require Alternative Routes")
                                         @DisplayName("Require Alternative Routes")
                                         @Optional @Placement(tab = ADVANCED_TAB)
                                         @Expression(ExpressionSupport.SUPPORTED)
                                         @ParameterDsl(allowReferences = false) boolean hasAlternatives,

                                         @Summary("Indicates that the calculated route(s) should avoid the indicated features")
                                         @DisplayName("Avoid Feature")
                                         @Optional @Placement(tab = ADVANCED_TAB)
                                         @Expression(ExpressionSupport.NOT_SUPPORTED)
                                         @ParameterDsl(allowReferences = false) AvoidRestriction avoid,

                                         @OfValues(LanguageValueProvider.class)
                                         @Summary("The language in which to return results")
                                         @DisplayName("Language")
                                         @Optional
                                         @Placement(tab = ADVANCED_TAB)
                                         @Expression(ExpressionSupport.SUPPORTED)
                                         @ParameterDsl(allowReferences = false) String language,

                                         @OfValues(RegionValueProvider.class)
                                         @Summary("Specifies the region code, specified as a ccTLD (\"top-level domain\") two-character value")
                                         @DisplayName("Region Biasing")
                                         @Optional
                                         @Placement(tab = ADVANCED_TAB)
                                         @Expression(ExpressionSupport.SUPPORTED)
                                         @ParameterDsl(allowReferences = false) String region,

                                         @Summary("Specifies the unit system to use when displaying results")
                                         @DisplayName("Unit System")
                                         @Optional
                                         @Placement(tab = ADVANCED_TAB)
                                         @Expression(ExpressionSupport.NOT_SUPPORTED)
                                         @ParameterDsl(allowReferences = false) UnitSystem unit,

                                         @Summary("Arrival or Departure Date")
                                         @DisplayName("Date Time")
                                         @Optional
                                         @Placement(tab = ADVANCED_TAB)
                                         @Expression(ExpressionSupport.NOT_SUPPORTED)
                                         @ParameterDsl(allowReferences = false) DateTimeParameter dateTime,

                                         @Summary("Specifies the assumptions to use when calculating time in traffic")
                                         @DisplayName("Traffic Model")
                                         @Optional
                                         @Placement(tab = ADVANCED_TAB)
                                         @Expression(ExpressionSupport.NOT_SUPPORTED)
                                         @ParameterDsl(allowReferences = false) TrafficType trafficModel,

                                         @Summary("Specifies one or more preferred modes of transit")
                                         @DisplayName("Transit Modes")
                                         @Optional
                                         @NullSafe
                                         @Placement(tab = ADVANCED_TAB)
                                         @Expression(ExpressionSupport.NOT_SUPPORTED)
                                         @ParameterDsl(allowReferences = false) Set<TransitModeParameter> transitModes,

                                         @Summary("Specifies preferences for transit routes")
                                         @DisplayName("Transit Routing Preference")
                                         @Optional
                                         @Placement(tab = ADVANCED_TAB)
                                         @Expression(ExpressionSupport.NOT_SUPPORTED)
                                         @ParameterDsl(allowReferences = false) TransitRoutingSetting transitRoutingPreference) {
        DirectionsResult result = new DirectionsService().getDirections(googleMapsConnection.getGeoApiContext(), origin, destination, travelMode,
                waypoints, hasAlternatives, avoid, language, region,  unit, dateTime, trafficModel, transitModes, transitRoutingPreference);
        return new ByteArrayInputStream(new Gson().toJson(result).getBytes(StandardCharsets.UTF_8));
    }
}
