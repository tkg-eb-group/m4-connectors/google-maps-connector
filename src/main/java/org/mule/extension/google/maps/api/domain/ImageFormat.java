/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.api.domain;

public enum ImageFormat {

    PNG("png"),
    PNG8("png8"),
    PNG32("png32"),
    GIF("gif"),
    JPG("jpg"),
    JPG_BASELINE("jpgBaseline");

    private final String format;

    ImageFormat(String format) {
        this.format = format;
    }

    public String getFormat() {
        return format;
    }
}
