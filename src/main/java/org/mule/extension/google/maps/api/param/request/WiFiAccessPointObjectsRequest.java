/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.api.param.request;

import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.dsl.xml.ParameterDsl;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

import java.io.Serializable;

public class WiFiAccessPointObjectsRequest implements Serializable {

    @Parameter
    @Summary("MAC Address")
    @DisplayName("MAC Address")
    @Expression(ExpressionSupport.SUPPORTED)
    @ParameterDsl(allowReferences = false)
    private String macAddress;

    @Parameter
    @Summary("Signal Strength")
    @DisplayName("Signal Strength")
    @Optional
    @Expression(ExpressionSupport.SUPPORTED)
    @ParameterDsl(allowReferences = false)
    private int signalStrength;

    @Parameter
    @Summary("Age")
    @DisplayName("Age")
    @Optional
    @Expression(ExpressionSupport.SUPPORTED)
    @ParameterDsl(allowReferences = false)
    private int age;

    @Parameter
    @Summary("Channel")
    @DisplayName("Channel")
    @Optional
    @Expression(ExpressionSupport.SUPPORTED)
    @ParameterDsl(allowReferences = false)
    private int channel;

    @Parameter
    @Summary("Signal to Noise Ratio")
    @DisplayName("Signal to Noise Ratio")
    @Optional
    @Expression(ExpressionSupport.SUPPORTED)
    @ParameterDsl(allowReferences = false)
    private int signalToNoiseRatio;

    public String getMacAddress() {
        return macAddress;
    }

    public int getSignalStrength() {
        return signalStrength;
    }

    public int getAge() {
        return age;
    }

    public int getChannel() {
        return channel;
    }

    public int getSignalToNoiseRatio() {
        return signalToNoiseRatio;
    }
}
