/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.internal.connection.provider;

import com.google.maps.*;
import com.google.maps.errors.ApiException;
import com.google.maps.model.LatLng;
import org.mule.extension.google.maps.internal.connection.GoogleMapsConnection;
import org.mule.extension.google.maps.internal.error.exception.GoogleMapsException;
import org.mule.extension.google.maps.internal.error.GoogleMapsErrorType;
import org.mule.runtime.api.connection.ConnectionValidationResult;
import org.mule.runtime.api.connection.PoolingConnectionProvider;

import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Example;
import org.mule.runtime.extension.api.annotation.param.display.Summary;
import org.mule.runtime.http.api.HttpConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class GoogleMapsConnectionProvider implements PoolingConnectionProvider<GoogleMapsConnection> {

  private static final Logger logger = LoggerFactory.getLogger(GoogleMapsConnectionProvider.class);

  @Parameter
  @Summary("Google Maps API Key")
  @DisplayName("API Key")
  @Expression(ExpressionSupport.SUPPORTED)
  @Example("1a2b3c4d5e6f7g8h9i10j")
  private String apiKey;

  public String getApiKey() {
    return apiKey;
  }

  @Override
  public GoogleMapsConnection connect() {
    GoogleMapsConnection connection = new GoogleMapsConnection(apiKey);
    try{
      connection.connect();
    } catch (Exception e) {
      logger.error("Error ocurred getting Google Maps connection", e);
      throw new GoogleMapsException("Unable to get Google Maps connection", GoogleMapsErrorType.CONNECTION_EXCEPTION,
              HttpConstants.HttpStatus.SERVICE_UNAVAILABLE);
    }
    return connection;
  }

  @Override
  public void disconnect(GoogleMapsConnection connection) {
    try {
      connection.invalidate();
    } catch (Exception e) {
      logger.error(String.format("Error while disconnecting Google Maps: %s", e.getMessage()), e);
      throw new GoogleMapsException("Unable to close Google Maps connection", GoogleMapsErrorType.CONNECTION_EXCEPTION,
              HttpConstants.HttpStatus.INTERNAL_SERVER_ERROR);

    }
  }

  @Override
  public ConnectionValidationResult validate(GoogleMapsConnection connection) {
    try {
      ElevationApi.getByPoint(connection.getGeoApiContext(), new LatLng(0,0)).await();
    } catch (ApiException | IOException e) {
      logger.error(String.format("Error while validating Google Maps: %s", e.getMessage()), e);
      return ConnectionValidationResult.failure("Connection is not valid", new GoogleMapsException("Unable to get Google Maps Connection", GoogleMapsErrorType.CONNECTION_EXCEPTION,
              HttpConstants.HttpStatus.SERVICE_UNAVAILABLE));
    } catch (InterruptedException ex){
      Thread.currentThread().interrupt();
      logger.error(String.format("Error while validating Google Maps: %s", ex.getMessage()), ex);
      return ConnectionValidationResult.failure("Connection is not valid", new GoogleMapsException("Unable to get Google Maps Connection", GoogleMapsErrorType.CONNECTION_EXCEPTION,
              HttpConstants.HttpStatus.SERVICE_UNAVAILABLE));
    }
      return ConnectionValidationResult.success();
  }
}
