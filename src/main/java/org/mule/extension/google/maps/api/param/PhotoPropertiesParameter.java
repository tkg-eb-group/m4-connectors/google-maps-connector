/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.api.param;

import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.dsl.xml.ParameterDsl;
import org.mule.runtime.extension.api.annotation.param.ExclusiveOptionals;
import org.mule.runtime.extension.api.annotation.param.Optional;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Example;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

@ExclusiveOptionals(isOneRequired = true)
public class PhotoPropertiesParameter {

    @Parameter
    @Summary("A string identifier that uniquely identifies a photo")
    @DisplayName("Photo Reference")
    @Example("CnRtAAAATLZNl354RwP_9UKbQ_5Psy40texXePv4oAlgP4qNEkdIrkyse7rPXYGd9D_Uj1rVsQdWT4oRz4QrYAJNpFX7rzqqMlZw2h2E2y5IKMUZ7ouD_SlcHxYq1yL4KbKUv3qtWgTK0A6QbGh87GB3sscrHRIQiG2RrmU_jF4tENr9wGS_YxoUSSDrYjWmrNfeEHSGSc3FyhNLlBU")
    @Expression(ExpressionSupport.SUPPORTED)
    @ParameterDsl(allowReferences = false)
    private String photoReference;

    @Parameter
    @Summary("Specifies the maximum desired height in pixels, of the image returned by the Place Photos service") @DisplayName("Max Height")
    @Example("400")
    @Optional
    @Expression(ExpressionSupport.SUPPORTED)
    @ParameterDsl(allowReferences = false)
    private Integer maxHeight;

    @Parameter
    @Summary("Specifies the maximum desired width in pixels, of the image returned by the Place Photos service") @DisplayName("Max Width")
    @Example("400")
    @Optional
    @Expression(ExpressionSupport.SUPPORTED)
    @ParameterDsl(allowReferences = false)
    private Integer maxWidth;

    public String getPhotoReference() {
        return photoReference;
    }

    public Integer getMaxHeight() {
        return maxHeight;
    }

    public Integer getMaxWidth() {
        return maxWidth;
    }
}
