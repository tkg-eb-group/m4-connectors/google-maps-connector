/**
 * (c) 2003-2021 MuleSoft, Inc. The software in this package is published under the terms of the Commercial Free Software license V.1 a copy of which has been included with this distribution in the LICENSE.md file.
 */
package org.mule.extension.google.maps.api.param;

import org.mule.runtime.api.meta.ExpressionSupport;
import org.mule.runtime.extension.api.annotation.Alias;
import org.mule.runtime.extension.api.annotation.Expression;
import org.mule.runtime.extension.api.annotation.dsl.xml.ParameterDsl;
import org.mule.runtime.extension.api.annotation.param.Parameter;
import org.mule.runtime.extension.api.annotation.param.display.DisplayName;
import org.mule.runtime.extension.api.annotation.param.display.Example;
import org.mule.runtime.extension.api.annotation.param.display.Summary;

@Alias("Waypoint")
public class WaypointParameter {

    @Parameter
    @Summary("Intermediate location to include along the route between the origin and destination points as pass through or stopover locations")
    @DisplayName("Waypoint")
    @Example("Sydney, Australia")
    @Expression(ExpressionSupport.SUPPORTED)
    @ParameterDsl(allowReferences = false)
    private String waypoint;

    public String getWaypoint() {
        return waypoint;
    }
}
