= Google Maps Connector
:keywords: anypoint studio, connector, endpoint


https://www.mulesoft.com/legal/versioning-back-support-policy#anypoint-connectors[_Certified_]

== Overview
The Google Maps connector was implemented using Java and consuming Google Maps Services v0.15.0 Java Client API. 

The connector exposes the following operations:

* Directions request
* Distance matrix request
* Elevation request
* Find place request
* Geocoding request
* Geolocation request
* Get static map
* Nearby search request
* Nearest roads request
* Photos request
* Place autocomplete request
* Place details request
* Query autocomplete request
* Retrieve address by place id
* Reverse geocoding request
* Snap to roads request
* Speed limits request
* Text search request
* Timezone request

== Prerequisites

[%header%autowidth]
|===
|Application/Service |Version
|Mule Runtime	     |  4.x
|Google Maps Services|  Google Maps Services API v0.15.0 Java Client
|Java	             |  1.8 and later
|===

This document assumes that you are familiar with Mule, Anypoint Connectors, Anypoint Studio, Mule concepts, elements in a Mule flow, and Global Elements.

You need the API Key to test your connection to your target resource.

For hardware and software requirements and compatibility
information, see the Connector Release Notes.

To use this connector with Maven, view the pom.xml dependency information in
the Dependency Snippets in Anypoint Exchange.

== Initial Steps to generate Google Maps API Key

Review https://developers.google.com/maps/documentation/directions/get-api-key to generate the Google Maps API Key to use and configure the Google Maps Connector. 

Once you have the API key you must enable in your project the following API's to allow the connector operations work properly:

- Directions API
- Elevation API
- Timezone API
- Geocoding API
- Distance Matrix API
- Geolocation API
- Places API
- Roads API (For Speed limits request, you also need an Asset Tracking License. For more information, review https://developers.google.com/maps/asset-tracking-plan)
- Static Maps API

== Configuration

image::./images/Google_Maps_Configuration.png[]

```xml
<google-maps:config name="Google_Maps_Configuration" doc:name="Google Maps Configuration" doc:id="374008e4-b409-4ddc-ae7e-4959554c3a19" >
		<google-maps:connection apiKey="xxxxxxxxxxxxxxxxxx"/>
</google-maps:config>
```
 
== Operations

For all operations properties, the required parameters are in the General tab and optional parameters in the Advanced tab for some operations. For most of the parameters it is enable to use an expression or a static value, except for enumerations or dropdown values.

=== Directions request

Operation that calculates directions between locations.

==== Required Parameters

Origin -> The place ID, address, or textual latitude/longitude value from which you wish to calculate directions.

Destination -> The place ID, address, or textual latitude/longitude value to which you wish to calculate directions.

==== Optional Parameters

Travel Mode —> Specifies the mode of transport to use when calculating directions. One of values: BICYCLING, DRIVING, TRANSIT, WALKING.

Waypoints —> Specifies a list of intermediate locations to include along the route between the origin and destination points as pass through or stopover locations. Waypoints alter a route by directing it through the specified location(s). Maximum number of waypoints supported is 25. 

Avoid Feature —> Indicates that the calculated route(s) should avoid the indicated features. One of values: FERRIES, HIGHWAYS, INDOOR, TOLLS.

Language —> The language in which to return results.

Region Biasing —> Specifies the region code, specified as a ccTLD ("top-level domain") two-character value.

Unit System —> Specifies the unit system to use when displaying results. One of values: IMPERIAL, METRIC.

Date Time —> Specifies the desired date time of arrival or departure for transit directions.

Traffic Model —> Specifies the assumptions to use when calculating time in traffic. One of values: BEST_GUESS, OPTIMISTIC, PESSIMISTIC.

Transit Mode —> Specifies one or more preferred modes of transit. One or more values: BUS, RAIL, SUBWAY, TRAIN, TRAM.

Transit Routing Preference —> Specifies preferences for transit routes. One of values: FEWER_TRANSFERS, LESS_WALKING.

==== Example

image::./images/Directions_Request.png[]

```xml
<google-maps:directions-request doc:name="Directions request" doc:id="090d7546-fb7c-4c91-b8f1-f451a662015d" config-ref="Google_Maps_Configuration">
            <google-maps:origin >
                <google-maps:address address="Toronto" />
            </google-maps:origin>
            <google-maps:destination >
                <google-maps:address address="Montreal" />
            </google-maps:destination>
</google-maps:directions-request>
```

For more information about Directions request, review https://developers.google.com/maps/documentation/directions/overview.

=== Distance matrix request

Operation that provides travel distance and time for a matrix of origins and destinations.

==== Required Parameters

Origins -> The starting point for calculating travel distance and time. You can supply one or more locations in the form of a place ID, an address, latitude/longitude coordinates or encoded polyline coordinates.

Destinations -> One or more locations to use as the finishing point for calculating travel distance and time. You can supply one or more locations in the form of a place ID, an address, latitude/longitude coordinates or encoded polyline coordinates.

==== Optional Parameters

Travel Mode —> Specifies the mode of transport to use when calculating directions. One of values: BICYCLING, DRIVING, TRANSIT, WALKING.

Avoid Feature —> ntroduces restrictions to the route. One of values: FERRIES, HIGHWAYS, INDOOR, TOLLS.

Language —> The language in which to return results.

Unit System —> Specifies the unit system to use when displaying results. One of values: IMPERIAL, METRIC.

Date Time —> Specifies the desired date time of arrival or departure for transit directions.

Traffic Model —> Specifies the assumptions to use when calculating time in traffic. One of values: BEST_GUESS, OPTIMISTIC, PESSIMISTIC.

Transit Mode —> Specifies one or more preferred modes of transit. One or more values: BUS, RAIL, SUBWAY, TRAIN, TRAM.

Transit Routing Preference —> Specifies preferences for transit routes. One of values: FEWER_TRANSFERS, LESS_WALKING.

=== Example

image::./images/Distance_Matrix_Request.png[]

```xml
<google-maps:distance-matrix-request doc:name="Distance matrix request" doc:id="1a4bd1e4-9a8c-4373-8055-a9e88548f157" config-ref="Google_Maps_Configuration" >
            <google-maps:origins >
                <google-maps:distance-address address="Vancouver BC" />
                <google-maps:distance-address address="Seattle" />
            </google-maps:origins>
            <google-maps:destinations >
                <google-maps:distance-address address="San Francisco" />
                <google-maps:distance-address address="Victoria BC" />
            </google-maps:destinations>
</google-maps:distance-matrix-request>
```

For more information about Distance Matrix request, review https://developers.google.com/maps/documentation/distance-matrix/overview.

=== Elevation request

Operation that provides a simple interface to query locations on the earth for elevation data.

==== Required Parameters

===== Positional Requests

Locations -> Defines the location(s) on the earth from which to return elevation data. You can supply one or more locations in the form of latitude/longitude coordinates or encoded polyline coordinates.

===== Sampled Path Requests

Path -> Defines a path on the earth for which to return elevation data. You can supply a path in the form of latitude/longitude coordinates or encoded polyline coordinates.
Samples -> Specifies the number of sample points along a path for which to return elevation data.

==== Example

image::./images/Elevation_Request.png[]

```xml
 <google-maps:elevation-request doc:name="Elevation request" doc:id="00bcec1f-5d29-4cb1-beb0-91de7de0d882" config-ref="Google_Maps_Configuration" >
            <google-maps:elevation-parameter >
                <google-maps:positional-request >
                    <google-maps:locations >
                        <google-maps:latitude-and-longitude-coordinate >
                            <google-maps:latitude-longitude-coordinates >
                                <google-maps:location latitude="39.7391536" longitude="-104.9847034" />
                                <google-maps:location latitude="36.455556" longitude="-116.866667" />
                            </google-maps:latitude-longitude-coordinates>
                        </google-maps:latitude-and-longitude-coordinate>
                    </google-maps:locations>
                </google-maps:positional-request>
            </google-maps:elevation-parameter>
</google-maps:elevation-request>
```

For more information about Elevation request, review https://developers.google.com/maps/documentation/elevation/overview.

=== Find place request

Operation that takes a text input and returns a place.

==== Required Parameters

Input Type —> The type of input. One of values: Phone number, Text query.

Query —> Text input that identifies the search target in case of selecting 'Text query' as Input Type.

Phone Number -> Phone number input in case of selecting 'Phone number' as Input Type.

==== Optional Parameters

Language —> The language code, indicating in which language the results should be returned, if possible.

Fields —> The fields specifying the types of place data to return. One or more values: BUSINESS_STATUS, FORMATTED_ADDRESS, GEOMETRY, ICON, ID, NAME, OPENING_HOURS, PHOTOS, PLACE_ID, PRICE_LEVEL, RATING, TYPES. 

Location Bias —> Prefer results in a specified area, by specifying either a radius plus lat/lng, or two lat/lng pairs representing the points of a rectangle. If this parameter is not specified, the API uses IP address biasing by default.

IP bias: Instructs the API to use IP address biasing.

Point: A single lat/lng coordinate.

Circular: A string specifying radius in meters, plus lat/lng in decimal degrees.

Rectangular: A string specifying two lat/lng pairs in decimal degrees, representing the south/west and north/east points of a rectangle.

==== Example

image::./images/Place_Request.png[]

image::./images/Place_Request_Advanced.png[]

```xml
 <google-maps:find-place-request doc:name="Find place request" doc:id="fe670396-bc25-46fc-9bc0-f24d537eb5ca" config-ref="Google_Maps_Configuration" >
            <google-maps:input-type >
                <google-maps:text-query query="Museum of Contemporary Art Australia" />
            </google-maps:input-type>
            <google-maps:places-fields >
                <google-maps:place-field searchPlaceField="PHOTOS" />
                <google-maps:place-field searchPlaceField="FORMATTED_ADDRESS" />
                <google-maps:place-field searchPlaceField="NAME" />
                <google-maps:place-field searchPlaceField="RATING" />
                <google-maps:place-field searchPlaceField="OPENING_HOURS" />
                <google-maps:place-field searchPlaceField="GEOMETRY" />
            </google-maps:places-fields>
        </google-maps:find-place-request>
```

For more information about Find place request, review https://developers.google.com/places/web-service/search#FindPlaceRequests.

=== Geocoding request

Operation for converting addresses into geographic coordinates, which you can use to place markers on a map, or position the map.

==== Required Parameters

Address —> The street address that you want to geocode.

Components -> A components filter with one or more elements.

==== Optional Parameters

Bounds —> The bounding box of the viewport within which to bias geocode results more prominently. This parameter will only influence, not fully restrict, results from the geocoder.

Language -> The language in which to return results.

Region -> The region code, specified as a ccTLD ("top-level domain") two-character value. 

==== Example

image::./images/Geocoding_Request.png[]

```xml
<google-maps:geocoding-request doc:name="Geocoding request" doc:id="3b29b31c-42e8-478d-a66b-a43ddd098854" config-ref="Google_Maps_Configuration" >
            <google-maps:components >
                <google-maps:component component="LOCALITY" value="santa cruz" />
                <google-maps:component component="COUNTRY" value="ES" />
            </google-maps:components>
</google-maps:geocoding-request>
```

For more information about Geocoding request, review https://developers.google.com/maps/documentation/geocoding/overview.

=== Geolocation request

Operation that returns a location and accuracy radius based on information about cell towers and WiFi nodes that the mobile client can detect.

==== Request Body

Home Mobile Country Code -> The mobile country code (MCC) for the device's home network.

Home Mobile Network Code -> The mobile network code (MNC) for the device's home network.

Radio Type -> The mobile radio type. One of values: lte, gsm, cdma, wcdma.

Carrier -> The carrier name.

Consider IP -> Specifies whether to fall back to IP geolocation if wifi and cell tower signals are not available.

Cell Towers -> An array of cell tower objects.

Wi-Fi Access Points -> An array of WiFi access point objects.

==== Example

image::./images/Geolocation_Request.png[]

image::./images/Geolocation_Request2.png[]

```xml
<google-maps:geolocation-request doc:name="Geolocation request" doc:id="1ded8ec9-55a6-4a89-a945-1015ba6e5538" config-ref="Google_Maps_Configuration" >
            <google-maps:geolocation-request-body homeMobileCountryCode="310" homeMobileNetworkCode="410" radioType="gsm" carrier="Vodafone" >
                <google-maps:cell-towers >
                    <google-maps:cell-tower-objects-request cellId="21532831" locationAreaCode="2862" mobileCountryCode="214" mobileNetworkCode="7" />
                </google-maps:cell-towers>
                <google-maps:wifi-access-points >
                    <google-maps:wi-fi-access-point-objects-request macAddress="00:25:9c:cf:1c:ac" signalStrength="-43" signalToNoiseRatio="0" />
                    <google-maps:wi-fi-access-point-objects-request macAddress="00:25:9c:cf:1c:ad" signalStrength="-55" signalToNoiseRatio="0" />
                </google-maps:wifi-access-points>
            </google-maps:geolocation-request-body>
</google-maps:geolocation-request>
```

For more information about Geolocation request, review https://developers.google.com/maps/documentation/geolocation/overview.

=== Get static map

Operation that creates your map based on URL parameters and returns the map as an image you can display on your web page.

==== Required Parameters

Width -> Defines width of the map image.

Height -> Defines height of the map image.

Center -> Defines the center of the map, equidistant from all edges of the map.

Zoom -> Defines the zoom level of the map, which determines the magnification level of the map.

Markers -> Define one or more markers to attach to the image at specified locations. 

Path -> Defines a single path of two or more connected points to overlay on the image at specified locations.

==== Optional Parameters

Scale -> Affects the number of pixels that are returned.

Image Format -> Defines the format of the resulting image. One of values: GIF, JPEG, JPEG_BASELINE, PNG, PNG32, PNG8.

Map Type -> Defines the type of map to construct. One of values: HYBRID, ROADMAP, SATELLITE, TERRAIN.

Language -> Defines the language to use for display of labels on map tiles.

Region -> Defines the appropriate borders to display, based on geo-political sensitivities. Accepts a region code specified as a two-character ccTLD ('top-level domain') value.

==== Example

image::./images/Static_Maps_Request.png[]

```xml
<google-maps:get-static-map doc:name="Get static map" doc:id="91e43594-7b8e-4825-acae-cd1aec53c369" config-ref="Google_Maps_Configuration" width="400" height="400" >
            <google-maps:location-parameters zoom="14" >
                <google-maps:center >
                    <google-maps:maps-location-address address="Berkeley,CA" />
                </google-maps:center>
            </google-maps:location-parameters>
</google-maps:get-static-map>
```

For more information about Get static map, review https://developers.google.com/maps/documentation/maps-static/overview.

=== Nearby search request

Operation that lets you search for places within a specified area.

==== Required Parameters

Location —> The latitude/longitude around which to retrieve place information.

Radius —> Defines the distance (in meters) within which to return place results.

==== Optional Parameters

Keyword —> A term to be matched against all content that Google has indexed for this place, including but not limited to name, type, and address, as well as customer reviews and other third-party content.

Language —> The language code, indicating in which language the results should be returned, if possible.

Min Price and Max Price —> Restricts results to only those places within the specified range. One of values: EXPENSIVE, FREE, INEXPENSIVE, MODERATE, VERY_EXPENSIVE.

Name —> A term to be matched against all content that Google has indexed for this place.

Open Now —> Returns only those places that are open for business at the time the query is sent.

Rank By —> Specifies the order in which results are listed. One of values: DISTANCE, PROMINENCE.

Place Type —> Restricts the results to places matching the specified type.

Page Token —> Returns up to 20 results from a previously run search.

==== Example

image::./images/Nearby_Search_Request.png[]

```xml
<google-maps:nearby-search-request doc:name="Nearby search request" doc:id="d86878c8-680f-4678-808e-626a3ebac734" config-ref="Google_Maps_Configuration" radius="1500" >
            <google-maps:location latitude="-33.8670522" longitude="151.1957362" />
</google-maps:nearby-search-request>
```

For more information about Nearby search request, review https://developers.google.com/places/web-service/search#PlaceSearchRequests.

=== Nearest roads request

Operation that returns individual road segments for a given set of GPS coordinates.

==== Required Parameters

Points -> A list of latitude/longitude pairs.

==== Example

image::./images/Nearest_Roads_Request.png[]

```xml
<google-maps:nearest-roads-request doc:name="Nearest roads request" doc:id="e27c077f-0ce6-4c63-b882-907b96531395" config-ref="Google_Maps_Configuration" >
            <google-maps:road-points >
                <google-maps:location latitude="60.170880" longitude="24.942795" />
                <google-maps:location latitude="60.170879" longitude="24.942796" />
                <google-maps:location latitude="60.170877" longitude="24.942796" />
            </google-maps:road-points>
</google-maps:nearest-roads-request>
```

For more information about Nearest Roads request, review https://developers.google.com/maps/documentation/roads/nearest.

=== Photos request

Operation that gives you access to the millions of photos stored in the Places database.

==== Required Parameters

Photo Reference -> A string identifier that uniquely identifies a photo. Photo references are returned from either a Place Search or Place Details request.

Max Height or Max Width —> Specifies the maximum desired height or width, in pixels, of the image returned by the Place Photos service.

==== Example

image::./images/Photos_Request.png[]

```xml
<google-maps:photos-request doc:name="Photos request" doc:id="5dc557bd-3a56-41e1-bef8-4d9af27e0505" config-ref="Google_Maps_Configuration" photoReference="CnRtAAAATLZNl354RwP_9UKbQ_5Psy40texXePv4oAlgP4qNEkdIrkyse7rPXYGd9D_Uj1rVsQdWT4oRz4QrYAJNpFX7rzqqMlZw2h2E2y5IKMUZ7ouD_SlcHxYq1yL4KbKUv3qtWgTK0A6QbGh87GB3sscrHRIQiG2RrmU_jF4tENr9wGS_YxoUSSDrYjWmrNfeEHSGSc3FyhNLlBU" maxWidth="400" />
```

For more information about Photos request, review https://developers.google.com/places/web-service/photos.

=== Place autocomplete request

Operation that returns place predictions in response.

==== Required Parameters

Input —> The text string on which to search.

==== Optional Parameters

Session Token —> A random string which identifies an autocomplete session for billing purposes. If this parameter is omitted from an autocomplete request, the request is billed independently.

Offset —> The position, in the input term, of the last character that the service uses to match predictions.

Origin — The origin point from which to calculate straight-line distance to the destination (returned as distance_meters).

Location — The point around which you wish to retrieve place information.

Radius — The distance (in meters) within which to return place results.

Language — The language code, indicating in which language the results should be returned, if possible.

Place Types — The types of place results to return.

Components — A grouping of places to which you would like to restrict your results.

Strict Bounds — Returns only those places that are strictly within the region defined by location and radius. This is a restriction, rather than a bias, meaning that results outside this region will not be returned even if they match the user input.

==== Example

image::./images/Place_Autocomplete_Request.png[]
image::./images/Place_Autocomplete_Request2.png[]
image::./images/Place_Autocomplete_Request3.png[]

```xml
<google-maps:place-autocomplete-request doc:name="Place autocomplete request" doc:id="5859ebdb-8a2f-4786-a2df-6426ec57f5dd" config-ref="Google_Maps_Configuration" input="Vict" language="fr" placeType="ESTABLISHMENT" isStrictBounds="true">
            <google-maps:place-location radius="500" >
                <google-maps:location latitude="37.76999" longitude="-122.44696" />
            </google-maps:place-location>
            <google-maps:components >
                <google-maps:component component="COUNTRY" value="us" />
            </google-maps:components>
</google-maps:place-autocomplete-request>
```

For more information about Place Autocomplete request, review https://developers.google.com/places/web-service/autocomplete.

=== Place details request

Operation that returns more details about a particular establishment or point of interest.

==== Required Parameters

Place Id -> A textual identifier that uniquely identifies a place, returned from a Place Search.

==== Optional Parameters

Language —> The language code, indicating in which language the results should be returned, if possible.

Region —> The region code, specified as a ccTLD (country code top-level domain) two-character value.

Session Token —> A random string which identifies an autocomplete session for billing purposes. Use this for Place Details requests that are called following an autocomplete request in the same user session.

Fields —> One or more fields, specifying the types of place data to return

==== Example

image::./images/Place_Details_Request.png[]
image::./images/Place_Details_Request2.png[]

```xml
<google-maps:place-details-request doc:name="Place details request" doc:id="e9aff0fe-c3f5-43a0-976c-b3710e72f2f9" config-ref="Google_Maps_Configuration" placeId="ChIJN1t_tDeuEmsRUsoyG83frY4" >
            <google-maps:places-details-fields >
                <google-maps:place-details-field placeDetailsField="NAME" />
                <google-maps:place-details-field placeDetailsField="FORMATTED_PHONE_NUMBER" />
                <google-maps:place-details-field placeDetailsField="RATING" />
            </google-maps:places-details-fields>
</google-maps:place-details-request>
```

For more information about Place Details request, review https://developers.google.com/places/web-service/details.

=== Query autocomplete request

Operation that can be used to provide a query prediction for text-based geographic searches, by returning suggested queries as you type.

==== Required Parameters

Input —> The text string on which to search.

==== Optional Parameters

Offset —> The character position in the input term at which the service uses text for predictions.

Location —> The point around which you wish to retrieve place information.

Radius —> The distance (in meters) within which to return place results.

Language —> The language code, indicating in which language the results should be returned, if possible.

==== Example

image::./images/Query_Autocomplete_Request.png[]
image::./images/Query_Autocomplete_Request2.png[]

```xml
<google-maps:query-autocomplete-request doc:name="Query autocomplete request" doc:id="6fa9353a-9851-4f74-91b9-cf1b643920e8" config-ref="Google_Maps_Configuration" input="pizza near par" offset="3" language="fr" />
```

For more information about Query Autocomplete request, review https://developers.google.com/places/web-service/query.

=== Retrieve address by place id

Operation to get an address from a given place id.

==== Required Parameters

Place Id -> The place ID of the place for which you wish to obtain the human-readable address.

==== Example

image::./images/Retrieve_Address_By_Place_Id_Request.png[]

```xml
<google-maps:retrieve-address-by-place-id doc:name="Retrieve address by place id" doc:id="ddf63d63-6631-49a4-9a7b-d74a706cef50" config-ref="Google_Maps_Configuration" placeId="ChIJd8BlQ2BZwokRAFUEcm_qrcA"/>
```

For more information about Retrieve Address By Place Id request, review https://developers.google.com/maps/documentation/geocoding/overview#place-id.

=== Reverse geocoding request

Operation for converting geographic coordinates into a human-readable address.

==== Required Parameters

Latitude and Longitude —> The latitude and longitude values specifying the location for which you wish to obtain the closest, human-readable address.

==== Optional Parameters

Address Types -> A filter of one or more address types. 

Location Types -> A filter of one or more location types.

Language —> The language in which to return results.

==== Example

image::./images/Reverse_Geocoding_Request.png[]
image::./images/Reverse_Geocoding_Request2.png[]

```xml
<google-maps:reverse-geocoding-request doc:name="Reverse geocoding request" doc:id="41d60ddd-31b7-48e0-9f80-53e1fcc7f243" config-ref="Google_Maps_Configuration" latitude="40.714224" longitude="-73.961452" language="es">
            <google-maps:addresses-types>
                <google-maps:address-type addressType="STREET_ADDRESS" />
            </google-maps:addresses-types>
            <google-maps:locations-types>
                <google-maps:location-type locationType="ROOFTOP" />
            </google-maps:locations-types>
</google-maps:reverse-geocoding-request>
```

For more information about Reverse Geocoding request, review https://developers.google.com/maps/documentation/geocoding/overview#ReverseGeocoding.

=== Snap to roads request

Operation that returns the best-fit road geometry for a given set of GPS coordinates.

==== Required Parameters

Path -> The path to be snapped.

==== Optional Parameters

Interpolate -> Whether to interpolate a path to include all points forming the full road-geometry. When true, additional interpolated points will also be returned, resulting in a path that smoothly follows the geometry of the road, even around corners and through tunnels. Interpolated paths will most likely contain more points than the original path.

==== Example

image::./images/Snap_To_Roads_Request.png[]

```xml
 <google-maps:snap-to-roads-request doc:name="Snap to roads request" doc:id="f5dbd2ae-561d-415c-b98b-f7843e3a3498" config-ref="Google_Maps_Configuration" isInterpolate="true" >
            <google-maps:road-paths >
                <google-maps:location latitude="-35.27801" longitude="149.12958" />
                <google-maps:location latitude="-35.28032" longitude="149.12907" />
                <google-maps:location latitude="-35.28099" longitude="149.12929" />
                <google-maps:location latitude="-35.28144" longitude="149.12984" />
                <google-maps:location latitude="-35.28194" longitude="149.13003" />
                <google-maps:location latitude="-35.28282" longitude="149.12956" />
                <google-maps:location latitude="-35.28302" longitude="149.12881" />
                <google-maps:location latitude="-35.28473" longitude="149.12836" />
            </google-maps:road-paths>
</google-maps:snap-to-roads-request>
```

For more information about Snap To Roads request, review https://developers.google.com/maps/documentation/roads/snap.

=== Speed limits request

Operation that returns the posted speed limit for a road segment.

==== Required Parameters

Path —> A list of up to 100 latitude/longitude pairs representing a path.

Place Id -> The place ID(s) representing one or more road segments.

==== Example

image::./images/Speed_Limits_Request.png[]

```xml
  <google-maps:speed-limits-request doc:name="Speed limits" doc:id="baa27656-c595-48dd-add2-e3fe32d9f53c" config-ref="Google_Maps_Configuration" >
            <google-maps:speed-limits-paths >
                <google-maps:road-paths >
                    <google-maps:location latitude="38.75807927603043" longitude="-9.03741754643809" />
                    <google-maps:location latitude="38.6896537" longitude="-9.1770515" />
                    <google-maps:location latitude="41.1399289" longitude="-8.6094075" />
                </google-maps:road-paths>
            </google-maps:speed-limits-paths>
  </google-maps:speed-limits-request>
```

For more information about Speed Limits request, review https://developers.google.com/maps/documentation/roads/speed-limits.

=== Text search request

Operation that returns information about a set of places based on a string.

==== Required Parameters

Query —> The text string on which to search.

==== Optional Parameters

Location -> The latitude/longitude around which to retrieve place information. If you specify a location parameter, you must also specify a radius parameter.

Region —> The region code, specified as a ccTLD (country code top-level domain) two-character value.

Language — The language code, indicating in which language the results should be returned, if possible.

Min Price and Max Price — Restricts results to only those places within the specified price level. One of values: EXPENSIVE, FREE, INEXPENSIVE, MODERATE, VERY_EXPENSIVE.

Open Now — Returns only those places that are open for business at the time the query is sent.

Page Token — Returns up to 20 results from a previously run search.

Place Type — Restricts the results to places matching the specified type.

==== Example

image::./images/Text_Search_Request.png[]
image::./images/Text_Search_Request2.png[]

```xml
<google-maps:text-search-request doc:name="Text search request" doc:id="c01751c1-d878-4d4f-b879-a4d82af8183c" config-ref="Google_Maps_Configuration" query="123 main street" region="mx" language="es" placeType="ESTABLISHMENT" >
            <google-maps:text-search-location radius="10000" >
                <google-maps:location latitude="42.3675294" longitude="-71.186966" />
            </google-maps:text-search-location>
</google-maps:text-search-request>
```

For more information about Text Search request, review https://developers.google.com/places/web-service/search#TextSearchRequests.

=== Timezone request

Operation that provides a simple interface to request the time zone for locations on the surface of the earth, as well as the time offset from UTC for each of those locations.

==== Required Parameters

Location -> A latitude-longitude coordinate representing the location to look up.

==== Example

image::./images/Timezone_Request.png[]

```xml
<google-maps:timezone-request doc:name="Timezone request" doc:id="30cab58b-203b-4c9d-96d1-3b9c138b7b0e" config-ref="Google_Maps_Configuration" latitude="39.6034810" longitude="-119.6822510" />
```

For more information about Timezone request, review https://developers.google.com/maps/documentation/timezone/overview.

== Useful Links

* Reference for Google Maps documentation: https://developers.google.com/maps/documentation
* To contact team : https://theksquaregroup.com/contact-us/[Ksquare]



  
